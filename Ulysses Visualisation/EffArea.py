import numpy as np
import matplotlib.pyplot as plt
import spiceypy as spice
from MiscFunctions import find_nearest_idx
from indices_dict import indices

#Units of spice are km and km/s

#Takes angles in degrees, not radians
def uniform(angle: float, interval: tuple = (-30,30)) -> float:
    if interval[0] <= angle and angle <= interval[1]:
        return 1/(interval[1]-interval[0])
    else:
        return 0

def avg_eff_area(eff_area: list, eff_area_angle: list, extra_angle: float) -> list:
    avg = []
    N = 360*10
    two_pi = np.linspace(0, 2*np.pi, num = N) #Rotation angle
    detector_meteoroid_angles = np.linspace(-85, 95, num = 181)
    for i, phi in enumerate(detector_meteoroid_angles):
        A = 0
        for n, theta in enumerate(two_pi):
            #dot_product = np.cos(phi/180*np.pi)+np.sin(phi/180*np.pi)*np.tan(extra_angle/180*np.pi)*np.cos(theta)
            dot_product = np.sin((phi+85)/180*np.pi)*np.tan(85/180*np.pi)*np.cos(theta)+np.cos((phi+85)/180*np.pi)
            norm = np.sqrt(1+np.tan(85/180*np.pi)**2)
            angle = np.arccos(dot_product/norm)/np.pi*180
            #index = find_nearest_idx(eff_area_angle, -2*detector_earth_angles[i]*np.cos(theta)+extra_angle+detector_earth_angles[i])
            index = find_nearest_idx(eff_area_angle, angle)
            #A += eff_area[index]/N*2*np.pi
            A += eff_area[index]/N
        avg.append(A)
    avg = np.array(avg)
    return avg

spice_path = '../../spice/'
spice.furnsh(spice_path + "naif0012.tls")
spice.furnsh(spice_path + "de440.bsp")
spice.furnsh(spice_path + "ulysses_1990_2009_2050.bsp")
one_year_et = spice.str2et('01-001T00:00')-spice.str2et('00-001T00:00')

start_et = spice.str2et('90-301T13:53')
end_et = spice.str2et('07-334T08:17')

num = 10000

index = []
time = []
lon = []
lat = []
ulysses_lat = []
with open('Ulysses_Data_File_Cleaned.txt') as cleaned_ulysses_data:
    for count, line in enumerate(cleaned_ulysses_data):
        if count >= indices['first_data_line']:
            line = line.split()
            time.append(line[indices['date']]+'T'+line[indices['time_of_day']])
            lon.append(float(line[indices['solar_lon_index']]))
            lat.append(float(line[indices['solar_lat_index']]))
            ulysses_lat.append(float(line[indices['LAT']]))

lon = np.array(lon)
lat = np.array(lat)

time = spice.str2et(time)

delta_t = (end_et-start_et)/num

max_area = 1000
extra_angle = 95

sensitivityRAW = np.loadtxt('sensitivity_function.txt', delimiter=',')
sensitivity_angle = np.concatenate((-np.flip(sensitivityRAW[1:,0]),sensitivityRAW[:,0]))
sensitivity_value = np.concatenate((np.flip(sensitivityRAW[1:,1]),sensitivityRAW[:,1]))

sensitivity = np.array([sensitivity_angle, sensitivity_value])


plt.ylabel(r'Area [cm$^2$]')
plt.xlabel('Angle between impactor and detector pointing [°]')
plt.title('Sensitivity function')
plt.plot(sensitivity[0,:], sensitivity[1,:])
plt.savefig('sensitivity.png')
plt.show()

uniformDist = []
for angle in sensitivity[0]:
    uniformDist.append(uniform(angle, interval = (-24,24)))
uniformDist = np.array(uniformDist)

plt.xlabel("Angle between impactor and detector pointing [°]")
plt.ylabel("Normalized Value")
plt.title("Uniform distribution")
plt.plot(sensitivity[0], uniformDist)
plt.savefig('uniform.png')
plt.show()

temp = avg_eff_area(sensitivity[1], sensitivity[0], extra_angle-10)
plt.title('Rotation averaged sensitivity function')
plt.ylabel(r'Area [cm$^2$]')
plt.xlabel('Angle between impactor and detector cone [°]')
#plt.plot(np.linspace(-85, 95, num = len(temp)), temp)
plt.plot(np.linspace(-85, 95, num = len(temp)), temp)
plt.savefig('rotationavg.png')
plt.show()


convolution = np.convolve(temp, uniformDist, mode='same')
conv_plt_angle = np.linspace(-len(convolution)/2,len(convolution)/2,len(convolution))
conv_plt_angle = np.linspace(-85, 95, num = len(temp))
plt.ylabel(r'Area [cm$^2$]')
plt.xlabel('Angle between impactor and detector cone [°]')
plt.title('Convolved sensitivity function')
#plt.plot(conv_plt_angle+extra_angle, convolution)
plt.plot(conv_plt_angle, convolution)
plt.savefig('convolved.png')
plt.show()

#convolution = sensitivity[1]
#conv_plt_angle = sensitivity[0]


wehry = np.loadtxt('DefaultDataset.csv', delimiter = ',')

velocity_dust = 30
for i in range(1):

    velocity_dust = 30+10*i
    
    angle = []
    angle_wrong = []
    factor = []
    UlLon = []
    EaLon = []
    LonTime = []
    for n, et in enumerate(time):
        [stateUlysses, ltime] = spice.spkezr('ULYSSES',  et,      'ECLIPJ2000', 'NONE', 'SUN')
        [stateEarth, ltime] = spice.spkezr('EARTH BARYCENTER',  et,      'ECLIPJ2000', 'NONE', 'SUN')
        velUlysses = stateUlysses[3:]
        posEarth = stateEarth[:3]
        posUlysses = stateUlysses[:3]
        dustVel = velocity_dust*posUlysses/(np.linalg.norm(posUlysses))
        relVel = velUlysses-dustVel
        pointingDetector = spice.latrec(1, lon[n]*2*np.pi/360, lat[n]*2*np.pi/360)
        factor.append(np.abs(np.linalg.norm(relVel)/np.linalg.norm(dustVel)))
        
        angle_wrong.append(-85+spice.vsep(velUlysses-dustVel, pointingDetector)*180/np.pi)
        
        angle.append(-85/180*np.pi+spice.vsep(velUlysses-dustVel, posEarth-posUlysses))
        
        if (ulysses_lat[n]<10 and ulysses_lat[n]>-10) and np.linalg.norm(posUlysses) < np.linalg.norm(posEarth)*2:
            val = ((spice.reclat(posEarth)[1]-spice.reclat(posUlysses)[1])%(np.pi))/np.pi*180
            EaLon.append(spice.reclat(posEarth)[1]/np.pi*180)
            UlLon.append(spice.reclat(posUlysses)[1]/np.pi*180)
            LonTime.append(et/one_year_et+2000)
            if val > 80 and val < 100:
                print(et/one_year_et+2000)
                print(spice.reclat(posEarth)[1]/np.pi*180, spice.reclat(posUlysses)[1]/np.pi*180, ulysses_lat[n])
    
    factor = np.array(factor)
    
    angle = np.array(angle)
    
    pltangle = angle*360/(2*np.pi)
    plttime = time/one_year_et+2000
    
    plt.scatter(LonTime, EaLon, color = 'blue')
    plt.scatter(LonTime, UlLon, color = 'red')
    plt.show()
    
    plt.title('Angle between Detector cone and beta meteoroid flux')
    plt.ylabel('Angle [°]')
    plt.xlabel('Time [years]')
    plt.plot(plttime, pltangle)
    plt.savefig('angles.png')
    plt.show()
    
    plt.plot(plttime, angle_wrong)
    plt.show()
    
    
    
    wehry_sensitivity = np.loadtxt('wehry_sensitivity.csv', delimiter = ',')
    
    eff_area = []
    for i in range(len(pltangle)):
        index = find_nearest_idx(conv_plt_angle, pltangle[i])
        eff_area.append(convolution[index])
        #index = find_nearest_idx(np.linspace(-95, 85, num = len(temp)), pltangle[i])
        #eff_area.append(temp[index])
    
    plt.title(str(velocity_dust) + 'km/s')
    plt.xlabel('Time [years]')
    plt.ylabel(r'Effective area [cm$^2$]')
    plt.plot(plttime, eff_area, label = 'Own estimate', color = 'red', ls = 'solid')
    plt.plot(wehry[:,0], wehry[:,1]*10000, label = 'Wehry', color = 'blue', ls = 'dashed')
    plt.legend()
    plt.savefig('raweffarea.png')
    plt.show()
    
    eff_area = np.array(eff_area)*factor



    plt.title(str(velocity_dust) + 'km/s')
    plt.xlabel('Time [years]')
    plt.ylabel(r'Velocity corrected effective area [cm$^2$]')
    plt.plot(plttime, eff_area, label = 'Own estimate', color = 'red', ls = 'solid')
    plt.plot(wehry[:,0], wehry[:,1]*10000, label = 'Wehry', color = 'blue', ls = 'dashed')
    plt.legend()
    plt.savefig('correffarea30.png')
    plt.show()
    
    strub = np.loadtxt('uly_beta_sensareaearly.txt', delimiter = ',')
    
    plt.title(str(velocity_dust) + 'km/s')
    plt.xlabel('Time [years]')
    plt.ylabel(r'Velocity corrected effective area [cm$^2$]')
    plt.plot(plttime, eff_area, label = 'Own estimate', color = 'red', ls = 'solid')
    plt.plot(strub[:,0], strub[:,1], label = 'Strub', color = 'blue', ls = 'dashed')
    plt.legend()
    plt.savefig('VergleichPeterTobias.png')
    plt.show()
    
    plt.title(str(velocity_dust) + 'km/s')
    plt.xlabel('Time [years]')
    plt.ylabel(r'Velocity corrected effective area [cm$^2$]')
    plt.plot(plttime, eff_area, label = 'Own estimate', color = 'red', ls = 'solid')
    plt.plot(strub[:,0], strub[:,1], label = 'Strub', color = 'blue', ls = 'dashed')
    plt.plot(wehry[:,0], wehry[:,1]*10000, label = 'Wehry', color = 'green', ls = '-.')
    plt.legend()
    plt.savefig('VergleichWehry.png')
    plt.show()
    
    
    eff_area_wrong = []
    convolution_wrong = np.convolve(sensitivity_value, uniformDist, mode='same')
    for i in range(len(pltangle)):
        index = find_nearest_idx(sensitivity_angle, angle_wrong[i])
        eff_area_wrong.append(convolution_wrong[index])
    
    eff_area_wrong = np.array(eff_area_wrong)*factor

    plt.plot(plttime, eff_area_wrong)
    plt.plot(plttime, eff_area)
    plt.plot(plttime, ulysses_lat)
    plt.show()
    
    with open(str(velocity_dust)+'.dat', 'w') as f:
        for i in range(len(eff_area)):
            f.write(str(plttime[i]) + ', ' + str(eff_area[i]) + '\n')


    plt.title(str(velocity_dust) + 'km/s')
    plt.plot(plttime, factor)
    plt.xlabel('Time [years]')
    plt.ylabel('Velocity factor')
    plt.savefig('factor.png')
    plt.show()
    
    
