import matplotlib.pyplot as plt
import numpy as np

from matplotlib.ticker import StrMethodFormatter

ISD = np.loadtxt('/home/steint/Documents/sens_isd_2d_fixed.txt', delimiter = ',')

beta = np.loadtxt('/home/steint/Documents/sens_wehry_2d_fixed.txt', delimiter = ',')

ISD_time = []
ISD_rest = []
for line in ISD[::10]:
    ISD_time.append(line[0])
    ISD_rest.append(line[1:])

beta_time = []
beta_rest = []
for line in beta[::10]:
    beta_time.append(line[0])
    beta_rest.append(line[1:])

ISD_rest = np.array(ISD_rest)
beta_rest = np.array(beta_rest)

# Create a new figure with a single subplot
fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(15, 4))  # Adjust the figure size as needed

# Plot the first image (ISD_rest) in the first subplot
ax1.imshow(ISD_rest)
ax1.set_title('ISD_rest')

# Plot the second image (beta_rest) in the second subplot
ax2.imshow(beta_rest)
ax2.set_title('beta_rest')

# Set custom y-axis ticks using the 'time_selected' array, displaying only every 100th y-tick
y_ticks_selected = ISD_time[::10]
ax1.set_yticks(np.arange(0, len(ISD_time), 10))
ax1.set_yticklabels(["{:.2f}".format(label) for label in y_ticks_selected])
ax2.set_yticks(np.arange(0, len(ISD_time), 10))
ax2.set_yticklabels(["{:.2f}".format(label) for label in y_ticks_selected])

ax3.imshow(ISD_rest*beta_rest)
ax3.set_title('mult')

ax3.set_yticks(np.arange(0, len(ISD_time), 10))
ax3.set_yticklabels(["{:.2f}".format(label) for label in y_ticks_selected])


# Adjust the space between subplots
plt.subplots_adjust(wspace=0.2)  # You can adjust the value to control the gap between subplots

# Show the combined figure with both images
plt.show()