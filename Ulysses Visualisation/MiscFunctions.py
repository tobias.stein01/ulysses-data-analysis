import numpy as np
import spiceypy as spice

#https://stackoverflow.com/questions/2566412/find-nearest-value-in-numpy-array
def find_nearest_idx(array: list, value: float) -> int:
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

def find_max_nested_structure(nested_structure: list) -> float:
    max_value = float('-inf')  # Initialize with negative infinity
    
    for item in nested_structure:
        if isinstance(item, (list, np.ndarray)):
            # If the item is a list or NumPy array, recursively find the maximum within that structure
            max_in_substructure = find_max_nested_structure(item)
            max_value = max(max_value, max_in_substructure)
        else:
            # If the item is neither a list nor a NumPy array, compare it to the current max_value
            max_value = max(max_value, item)
    
    return max_value

def uly_Direction2rotAngle(axis, direction):
    north = np.array([0, 0, 1])

    if len(direction) != 3:
        print("uly_Direction2rotAngle: 3D-Vector required as input direction!")
        return None

    e_axis = axis / np.linalg.norm(axis)
  
    plane_y = np.cross(north, e_axis)
    plane_y /= np.linalg.norm(plane_y)
    
    plane_x = np.cross(e_axis, plane_y)
    plane_x /= np.linalg.norm(plane_x)
  
    direc_x = np.dot(plane_x, direction)
    direc_y = np.dot(plane_y, direction)
 
    rotang = (np.degrees(np.arctan2(direc_y, direc_x)) + 360) % 360

    direc2 = tmp_uly_rotAngle2Direction(e_axis, rotang)
    angle = vec_angle(direction, direc2)

    return rotang


def tmp_uly_rotAngle2Direction(axis, rotangle):
    detangle = 95
    north = np.array([0, 0, 1])
    e_axis = axis / np.linalg.norm(axis)
  
    plane_y = np.cross(north, e_axis)
    plane_y /= np.linalg.norm(plane_y)
    
    plane_x = np.cross(e_axis, plane_y)
    plane_x /= np.linalg.norm(plane_x)

    plane_vec = plane_x * np.cos(np.radians(rotangle)) + plane_y * np.sin(np.radians(rotangle))
            
    det_vec = e_axis * np.cos(np.radians(detangle)) + plane_vec * np.sin(np.radians(detangle))

    return det_vec


def vec_angle(vec1, vec2):
    dot_product = np.dot(vec1, vec2)
    norms_product = np.linalg.norm(vec1) * np.linalg.norm(vec2)
    cos_angle = dot_product / norms_product
    angle = np.degrees(np.arccos(np.clip(cos_angle, -1.0, 1.0)))
    return angle

#Die Funktion zum berechnen des gewichteten arithmetischen Mittelwerts (weighted arithmetic mean)
def wam(x,sigma): #Input ist ein array mit den Werten und ein array mit den Fehlern
    n = len(x)
    invSigmaMean2 = 0
    for i in range(n): #Loop zur Berechnung von 1/sigma^2
        invSigmaMean2 += 1/sigma[i]**2
    sigmaMean2 = 1/invSigmaMean2
    xMean = 0
    for i in range(n): #Loop zur Berechnung vom gewichteten Mittelwert
        xMean += x[i]/sigma[i]**2
    xMean *= sigmaMean2
    return xMean,np.sqrt(sigmaMean2) #Output ist der gewichtete Mittelwert und sein Fehler