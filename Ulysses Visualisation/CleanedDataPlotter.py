##Things to do
#Check if Peters stream times are correct
#A future master thesis would simulate ISD fluxes, their plasma clouds, their rise times and charge yields, and therefore get simulated wall impact vs non wall impact data and compare that to the measurements
#Markus Fraenz fragen wegen symIon. Wie einfach ist den Ulysses Dust Detector zu simulieren
#Remove streams by filtering put low intensity classes
#Do some error propagation
#Do dofferent binning for flux

import numpy as np
import matplotlib.pyplot as plt
from indices_dict import indices
import spiceypy as spice
from MiscFunctions import find_nearest_idx, find_max_nested_structure, wam
import matplotlib.lines as mlines
from scipy.stats import poisson
from scipy.signal import argrelextrema

class CleanedDataPlotter:
    
    def execute_wehry(self):
        self._remove_999_values()
        self._set_angles_and_velocities()
        self._plot_wehry(self._detector_sun_angles, self._velocities)        
        self._choose_beta_meteoroids()
        self._compare_ISD_and_beta()
        self._plot_wehry(self._beta_angles, self._beta_vel)
        self._effective_area()
        self._minimum_effective_area_hist()
        self._set_new_angles_and_velocities()
        
        et = self.data[:, self._time_index]
        distance = []
        for i in range(len(self.data[:,0])):
            [JupiterUlysses, ltime] = spice.spkezr('ULYSSES',  et[i],      'ECLIPJ2000', 'NONE', 'JUPITER_BARYCENTER')
            distance.append(np.linalg.norm(JupiterUlysses))
        distance = np.array(distance)/149600000
        plt.plot(et/self.one_year_et+2000,distance)
        plt.xlabel('Year')
        plt.ylabel('Distance between Ulysses and Jupiter [au]')
        plt.show()
        print(distance[argrelextrema(distance, np.less)])
        
        #self._plot_wehry_time_separated(self._new_detector_sun_angles, self._new_velocities)
        self._compare_found_betas()
        self._plot_lat()
        self._plot_zodiac_direction()
        self._set_angles_and_velocities()
        self._calculate_zero_crossings_times()
        self._count_and_print_north_fraction()
        self._plot_effective_area_with_streams()
        self._plot_flux()
        
    def _remove_999_values(self):
        ulysses_data_without_999 = []
        ulysses_data_only_999 = []
        for i in range(len(self.data[:, self.rotation_angle_index])):
            condition = self.data[i, self.rotation_angle_index] != 999 and self.data[i, self.quality_flag_index] >= self.min_quality_flag
            if condition:
                ulysses_data_without_999.append(self.data[i])
            else:
                ulysses_data_only_999.append(self.data[i])
        self.data_without_999 = np.array(ulysses_data_without_999)
        self.data_only_999 = np.array(ulysses_data_only_999) #ALSO INCLUDES QUALITY FLAG FILTERING
        print(len(self.data_without_999), 'no 999', len(self.data_only_999) ,'999')

    def _set_angles_and_velocities(self):
        self._detector_sun_angles, self._velocities = self._calculate_angle_and_velocities_between_two_objects(self.data_without_999)
    
    def _calculate_angle_and_velocities_between_two_objects(self, data: list) -> (list, list):
        
        lon = data[:, indices['solar_lon_index']]
        lat = data[:, indices['solar_lat_index']]
        detector_sun_angles = []
        velDust = []
        velUlysses0 = []
        et = data[:, self._time_index]
        for i in range(len(data[:,0])):
            [stateUlysses, ltime] = spice.spkezr('ULYSSES',  et[i],      'ECLIPJ2000', 'NONE', 'SUN')
            posUlysses = stateUlysses[:3]
            velUlysses = stateUlysses[3:]
            velUlysses0.append(np.linalg.norm(velUlysses))
 
            velImpact = data[i, indices['velocity_index']]
            
            pointingDetector = spice.latrec(1, lon[i]*2*np.pi/360, lat[i]*2*np.pi/360)
            
            vel = velUlysses-velImpact*pointingDetector
            velDust.append(np.linalg.norm(vel))
            
            
            #angle0 = spice.vsep(vel, [0,0,1])*360/(2*np.pi)
            angle0 = spice.vsep(vel, posUlysses)*360/(2*np.pi)
            detector_sun_angles.append(angle0)
            
            """
            e_r = np.ones(3)/np.sqrt(3)
            scalar_product = np.dot(e_r, vel)
            
            velDust.append(scalar_product)
            """                
            
        return detector_sun_angles, velDust
        

    def _plot_wehry(self, angles: list, velocities: list):
        plt.figure()
        plt.xlabel('Angle between dust flow and sun in solar reference frame [°]')
        plt.ylabel('Particle Velocity [km/s]')
        plt.scatter(angles,velocities, marker = '.', s = 1)        
        plt.plot(self._detector_sun_angles, np.ones(len(self._detector_sun_angles))*self._wehry_velocity, color = 'red')
        plt.plot(np.ones(len(self._detector_sun_angles))*self._wehry_angle, self._velocities, color = 'red')
        if not self._one_time_wehry_flag:
            plt.savefig('wehry.pdf')
        else:
            plt.savefig('wehry1.pdf')
        plt.show()
        
        if not self._one_time_wehry_flag:
            
            self._one_time_wehry_flag = True
        
            angles = np.array(angles)
            velocities = np.array(velocities)
            
            Jupiter_Time = [ 1991.727 , 1992.824 ]
            
            index_ecliptic_indices = self.data_without_999[np.where(self.data_without_999[:,self._time_index]/self.one_year_et+2000<Jupiter_Time[0]),self._index_index][0]
            index_ecliptic_indices = [int(i-0.1) for i in index_ecliptic_indices]
            index_post_ecliptic_indices = self.data_without_999[np.where(self.data_without_999[:,self._time_index]/self.one_year_et+2000>Jupiter_Time[1]),self._index_index][0]
            index_post_ecliptic_indices = [int(i-0.1) for i in index_post_ecliptic_indices]
            
            print(index_ecliptic_indices)
            
            ecliptic_indices = np.where(self.data_without_999[:,self._time_index]/self.one_year_et+2000<Jupiter_Time[0])
            post_ecliptic_indices = np.where(self.data_without_999[:,self._time_index]/self.one_year_et+2000>Jupiter_Time[1])
            ecliptic_angles = angles[ecliptic_indices]
            post_ecliptic_angles = angles[post_ecliptic_indices]
            ecliptic_vel = velocities[ecliptic_indices]
            post_ecliptic_vel = velocities[post_ecliptic_indices]
            
            
            
            
            x1 = 33
            y1 = 20
            x2 = 50
            y2 = 33
            delta = 4
            delta = 8
    
            m = (y2-y1)/(x2-x1)
            b = y1-m*x1
            
            ineterestingPopulation = []
            ineterestingPopulation2 = []
            interesting_indices = []
            interesting_indices2 = []
            index_interesting_indices = []
            index_interesting_indices2 = []
            for i in range(len(ecliptic_angles)):
                #if np.abs(velocities[i]-m*angles[i]-b)<delta and angles[i] < 50 and velocities[i] > 20:
                if np.abs(velocities[i]-m*angles[i]-b)<delta:
                    index_interesting_indices.append(index_ecliptic_indices[i])
                    interesting_indices.append(i)
                    ineterestingPopulation.append(self.data_without_999[i])
                if velocities[i]>m*angles[i]+b+delta and angles[i]<50 and velocities[i]>20:
                    index_interesting_indices2.append(index_ecliptic_indices[i])
                    interesting_indices2.append(i)
                    ineterestingPopulation2.append(self.data_without_999[i])
            ineterestingPopulation = np.array(ineterestingPopulation)
            ineterestingPopulation2 = np.array(ineterestingPopulation2)
            self._interesting_indices = interesting_indices
            
            plt.figure()
            plt.xlabel('Angle between dust flow and sun in solar reference frame [°]')
            plt.ylabel('Particle Velocity [km/s]')
            plt.scatter(ecliptic_angles,ecliptic_vel, marker = 'x', s = 10, label = 'before Jupiter')
            plt.scatter(post_ecliptic_angles,post_ecliptic_vel, marker = '+', s = 1, label = 'after Jupiter fly-by')        
            plt.scatter(ecliptic_angles[interesting_indices],ecliptic_vel[interesting_indices], marker = 'x', s = 10, label = 'diagonal before Jupiter')
            plt.scatter(ecliptic_angles[interesting_indices2],ecliptic_vel[interesting_indices2], marker = 'x', s = 10, label = 'interesting population')
            plt.plot(self._detector_sun_angles, np.ones(len(self._detector_sun_angles))*self._wehry_velocity, color = 'red')
            plt.plot(np.ones(len(self._detector_sun_angles))*self._wehry_angle, self._velocities, color = 'red')
            plt.legend()
            plt.savefig('diagonal.pdf')
            plt.show()
            
            plt.figure()
            plt.xlabel('Angle between dust flow and sun in solar reference frame [°]')
            plt.ylabel('Particle Speed in Ulysses reference frame [km/s]')
            plt.scatter(ecliptic_angles,self.data[index_ecliptic_indices, indices['velocity_index']], marker = 'x', s = 10, label = 'before Jupiter')
            plt.scatter(post_ecliptic_angles,self.data[index_post_ecliptic_indices, indices['velocity_index']], marker = '+', s = 1, label = 'after Jupiter fly-by')        
            plt.scatter(ecliptic_angles[interesting_indices],self.data[index_interesting_indices, indices['velocity_index']], marker = 'x', s = 10, label = 'diagonal before Jupiter')
            plt.scatter(ecliptic_angles[interesting_indices2],self.data[index_interesting_indices2, indices['velocity_index']], marker = 'x', s = 10, label = 'interesting population')
            plt.plot(self._detector_sun_angles, np.ones(len(self._detector_sun_angles))*self._wehry_velocity, color = 'red')
            plt.plot(np.ones(len(self._detector_sun_angles))*self._wehry_angle, self._velocities, color = 'red')
            plt.legend()
            plt.tight_layout()
            #plt.savefig('diagonal.pdf')
            plt.show()
            
            lonlow = np.where(self.data_without_999[ecliptic_indices,indices['solar_lon_index']][0] < 150)
            lonhigh = np.where(self.data_without_999[ecliptic_indices,indices['solar_lon_index']][0] > 150)
            
            plt.figure()
            plt.xlabel('Angle between dust flow and sun in solar reference frame [°]')
            plt.ylabel('Particle Velocity [km/s]')
            plt.scatter(ecliptic_angles[lonlow],ecliptic_vel[lonlow], marker = 'x', s = 10, label = 'before Jupiter LON < 150')
            plt.scatter(post_ecliptic_angles,post_ecliptic_vel, marker = '+', s = 1, label = 'after Jupiter fly-by')        
            plt.scatter(ecliptic_angles[lonhigh],ecliptic_vel[lonhigh], marker = 'x', s = 10, label = 'before Jupiter LON > 150')
            plt.plot(self._detector_sun_angles, np.ones(len(self._detector_sun_angles))*self._wehry_velocity, color = 'red')
            plt.plot(np.ones(len(self._detector_sun_angles))*self._wehry_angle, self._velocities, color = 'red')
            plt.legend()
            plt.savefig('diagonalSplit.pdf')
            plt.show()
            
            s_here = 5
            
            plt.scatter(self.data_without_999[ecliptic_indices,self._time_index]/self.one_year_et+2000, self.data_without_999[ecliptic_indices,indices['LAT']], label = 'before Jupiter', s = s_here)
            plt.scatter(ineterestingPopulation[:,self._time_index]/self.one_year_et+2000, ineterestingPopulation[:,indices['LAT']], label = 'diagonal before Jupiter', s = s_here)
            plt.scatter(ineterestingPopulation2[:,self._time_index]/self.one_year_et+2000, ineterestingPopulation2[:,indices['LAT']], label = 'interesting population', s = s_here)
            plt.xlabel('Year')
            plt.ylabel('Ulysses Ecliptic Latitude [°]')
            plt.legend()
            plt.show()
            
            plt.scatter(self.data_without_999[ecliptic_indices,self._time_index]/self.one_year_et+2000, self.data_without_999[ecliptic_indices,indices['IA']], label = 'before Jupiter', s = s_here)
            plt.scatter(ineterestingPopulation[:,self._time_index]/self.one_year_et+2000, ineterestingPopulation[:,indices['IA']], label = 'diagonal before Jupiter', s = s_here)
            plt.scatter(ineterestingPopulation2[:,self._time_index]/self.one_year_et+2000, ineterestingPopulation2[:,indices['IA']], label = 'interesting population', s = s_here)
            plt.xlabel('Year')
            plt.ylabel('Ion Amplitude')
            plt.legend()
            plt.savefig('IA.pdf')
            plt.show()
            
            plt.scatter(self.data_without_999[ecliptic_indices,self._time_index]/self.one_year_et+2000, self.data_without_999[ecliptic_indices,indices['solar_lon_index']], label = 'before Jupiter', s = s_here)
            plt.scatter(ineterestingPopulation[:,self._time_index]/self.one_year_et+2000, ineterestingPopulation[:,indices['solar_lon_index']], label = 'diagonal before Jupiter', s = s_here)
            plt.scatter(ineterestingPopulation2[:,self._time_index]/self.one_year_et+2000, ineterestingPopulation2[:,indices['solar_lon_index']], label = 'interesting population', s = s_here)
            plt.xlabel('Year')
            plt.ylabel('Detector Pointing Longitude')
            plt.legend()
            plt.savefig('diagoinalLongitude.pdf')
            plt.show()
            
            plt.scatter(self.data_without_999[ecliptic_indices,self._time_index]/self.one_year_et+2000, self.data_without_999[ecliptic_indices,indices['solar_lat_index']], label = 'before Jupiter', s = s_here)
            plt.scatter(ineterestingPopulation[:,self._time_index]/self.one_year_et+2000, ineterestingPopulation[:,indices['solar_lat_index']], label = 'diagonal before Jupiter', s = s_here)
            plt.scatter(ineterestingPopulation2[:,self._time_index]/self.one_year_et+2000, ineterestingPopulation2[:,indices['solar_lat_index']], label = 'interesting population', s = s_here)
            plt.xlabel('Year')
            plt.ylabel('Detector Pointing Latitude')
            plt.legend()
            plt.savefig('solLatInt.pdf')
            plt.show()
        
        
        
    def _plot_wehry_time_separated(self, angles: list, velocities: list):
        jupiter_flyby_et = -2.47398962e+08
        first_orbit_start_et = spice.str2et('1992-02-01')
        first_orbit_end_et = spice.str2et('1995-08-01')
        second_orbit_start_et = spice.str2et('1995-08-16')
        second_orbit_end_et = spice.str2et('2001-10-19')
        first_orbit_north_start_et = spice.str2et('1995-02-01')
        first_orbit_north_end_et = spice.str2et('1995-08-01')
        first_orbit_south_start_et = spice.str2et('1992-02-01')
        first_orbit_south_end_et = spice.str2et('1995-02-01')
        
        angles = np.array(angles)
        velocities = np.array(velocities)
        
        xlim = 120
        ylim = 100
        plt.xlabel('Angle between dust flow and sun in solar reference frame [°]')
        plt.ylabel('Particle Velocity [km/s]')
        plt.title('Pre fly-by')
        pltangles = angles[np.array(np.where(self.data[:,self._time_index] < jupiter_flyby_et))[0]]
        pltvelocities = velocities[np.array(np.where(self.data[:,self._time_index] < jupiter_flyby_et))[0]]
        plt.scatter(pltangles,pltvelocities)
        plt.plot(np.linspace(0, xlim, num = ylim), np.ones(ylim)*self._wehry_velocity, color = 'red')
        plt.plot(np.ones(xlim)*self._wehry_angle, np.linspace(0, ylim, num = xlim), color = 'red')
        plt.xlim(0,120)
        plt.ylim(0,100)
        plt.show()
        
        xlim = 200
        ylim = 100
        plt.xlabel('Angle between dust flow and sun in solar reference frame [°]')
        plt.ylabel('Particle Velocity [km/s]')
        plt.title('First orbit')
        pltangles = angles[np.array(np.where((self.data[:,self._time_index] > first_orbit_start_et) & (self.data[:,self._time_index] < first_orbit_end_et)))[0]]
        pltvelocities = velocities[np.array(np.where((self.data[:,self._time_index] > first_orbit_start_et) & (self.data[:,self._time_index] < first_orbit_end_et)))[0]]
        plt.scatter(pltangles,pltvelocities)
        plt.plot(np.linspace(0, xlim, num = ylim), np.ones(ylim)*self._wehry_velocity, color = 'red')
        plt.plot(np.ones(xlim)*self._wehry_angle, np.linspace(0, ylim, num = xlim), color = 'red')
        plt.xlim(0,200)
        plt.ylim(0,100)
        plt.show()
        
        xlim = 200
        ylim = 100
        plt.xlabel('Angle between dust flow and sun in solar reference frame [°]')
        plt.ylabel('Particle Velocity [km/s]')
        plt.title('First orbit outside ecliptic')
        pltangles = angles[np.array(np.where((self.data[:,self._time_index] > first_orbit_start_et) & (self.data[:,self._time_index] < first_orbit_end_et) & (np.abs(self.data[:,indices['LAT']]) >= 18)))[0]]
        pltvelocities = velocities[np.array(np.where((self.data[:,self._time_index] > first_orbit_start_et) & (self.data[:,self._time_index] < first_orbit_end_et) & (np.abs(self.data[:,indices['LAT']]) >= 18)))[0]]
        plt.scatter(pltangles,pltvelocities)
        plt.plot(np.linspace(0, xlim, num = ylim), np.ones(ylim)*self._wehry_velocity, color = 'red')
        plt.plot(np.ones(xlim)*self._wehry_angle, np.linspace(0, ylim, num = xlim), color = 'red')
        plt.xlim(0,200)
        plt.ylim(0,100)
        plt.show()
        
        xlim = 200
        ylim = 80
        plt.xlabel('Angle between dust flow and sun in solar reference frame [°]')
        plt.ylabel('Particle Velocity [km/s]')
        plt.title('Second orbit')
        pltangles = angles[np.array(np.where((self.data[:,self._time_index] > second_orbit_start_et) & (self.data[:,self._time_index] < second_orbit_end_et)))[0]]
        pltvelocities = velocities[np.array(np.where((self.data[:,self._time_index] > second_orbit_start_et) & (self.data[:,self._time_index] < second_orbit_end_et)))[0]]
        plt.scatter(pltangles,pltvelocities)
        plt.plot(np.linspace(0, xlim, num = ylim), np.ones(ylim)*self._wehry_velocity, color = 'red')
        plt.plot(np.ones(xlim)*self._wehry_angle, np.linspace(0, ylim, num = xlim), color = 'red')
        plt.xlim(0,200)
        plt.ylim(0,80)
        plt.show()
        
        xlim = 100
        ylim = 100
        plt.xlabel('Angle between dust flow and sun in solar reference frame [°]')
        plt.ylabel('Particle Velocity [km/s]')
        plt.title('First orbit north')
        pltangles = angles[np.array(np.where((self.data[:,self._time_index] > first_orbit_north_start_et) & (self.data[:,self._time_index] < first_orbit_north_end_et)))[0]]
        pltvelocities = velocities[np.array(np.where((self.data[:,self._time_index] > first_orbit_north_start_et) & (self.data[:,self._time_index] < first_orbit_north_end_et)))[0]]
        plt.scatter(pltangles,pltvelocities)
        plt.plot(np.linspace(0, xlim, num = ylim), np.ones(ylim)*self._wehry_velocity, color = 'red')
        plt.plot(np.ones(xlim)*self._wehry_angle, np.linspace(0, ylim, num = xlim), color = 'red')
        plt.xlim(0,100)
        plt.ylim(0,100)
        plt.show()
        
        xlim = 200
        ylim = 100
        plt.xlabel('Angle between dust flow and sun in solar reference frame [°]')
        plt.ylabel('Particle Velocity [km/s]')
        plt.title('First orbit south')
        pltangles = angles[np.array(np.where((self.data[:,self._time_index] > first_orbit_south_start_et) & (self.data[:,self._time_index] < first_orbit_south_end_et)))[0]]
        pltvelocities = velocities[np.array(np.where((self.data[:,self._time_index] > first_orbit_south_start_et) & (self.data[:,self._time_index] < first_orbit_south_end_et)))[0]]
        plt.scatter(pltangles,pltvelocities)
        plt.plot(np.linspace(0, xlim, num = ylim), np.ones(ylim)*self._wehry_velocity, color = 'red')
        plt.plot(np.ones(xlim)*self._wehry_angle, np.linspace(0, ylim, num = xlim), color = 'red')
        plt.xlim(0,200)
        plt.ylim(0,100)
        plt.show()

    def _choose_beta_meteoroids(self) -> list:
        
        mass = self.data_without_999[:, indices['mass_index']]
        
        
        lon = self.data_without_999[:, indices['solar_lon_index']]
        lat = self.data_without_999[:, indices['solar_lat_index']]
        et = self.data_without_999[:, self._time_index]
        
        beta_meteoroids = []
        beta_angles = []
        beta_vel = []
        
        ISD_direction = spice.latrec(1, self._interstellar_ecliptic_lon*2*np.pi/360, self._interstellar_ecliptic_lat*2*np.pi/360)
        detector_ISD_angles = []
        velDust = []
        et = self.data_without_999[:, self._time_index]
        for i in range(len(self.data_without_999[:,0])):
            [stateUlysses, ltime] = spice.spkezr('ULYSSES',  et[i],      'ECLIPJ2000', 'NONE', 'SUN')
            velUlysses = stateUlysses[3:]
            velImpact = self.data_without_999[i, indices['velocity_index']]
            pointingDetector = spice.latrec(1, lon[i]*2*np.pi/360, lat[i]*2*np.pi/360)
            vel = velUlysses-velImpact*pointingDetector
            detector_ISD_angles.append(spice.vsep(vel, ISD_direction)*360/(2*np.pi))
            velDust.append(np.linalg.norm(vel))
        detector_ISD_angles = np.array(detector_ISD_angles)
        
        # Your existing plot code
        fig, ax1 = plt.subplots()
        
        ax1.scatter(et/self.one_year_et+2000, -detector_ISD_angles+180, s = 1)
        ax1.plot(et/self.one_year_et+2000, self._tolerance*np.ones(len(detector_ISD_angles)), color='red')
        ax1.set_xlabel('Year')
        ax1.set_ylabel('Angle between dust velocity and ISD [°]')
        
        # Create a twin Axes for the second x-axis at the top
        ax2 = ax1.twiny()
        
        flyby = 1992.17646473
        aphelia = [1998.28180604, 2004.52656168]
        maxima = [1995+238/365.2425, 2001+303.5/365.2425]
        minima = [1994+281/365.2425, 2000+353/365.2425, 2007+69/365.2425]
        
        
        # Add specific times to the top x-axis
        specific_times = [flyby, minima[0], maxima[0], aphelia[0], minima[1], maxima[1], aphelia[1], minima[2]]  # Add the specific times you want to annotate
        specific_phrases = ['Jupiter Fly-By', 'South Pole', 'North Pole', 'Aphelion', 'South Pole', 'North Pole', 'Aphelion', 'South Pole']  # Corresponding phrases
        
        ax2.set_xticks(specific_times)
        ax2.set_xticklabels(specific_phrases, rotation=45, ha='left')  # Adjust rotation and ha
        
        ax2.set_xlim(ax1.get_xlim())
        
        plt.tight_layout()
        plt.savefig('ISD.pdf')
        plt.show()
        
        velDust = np.array(velDust)
        mass = np.array(mass)
        
        condition_23 = []
        condition_01 = []
        for i in range(len(mass)):
            condition_not_streams = True
            for interval in self._tobias_streams:
                if et[i]/self.one_year_et+2000 >= interval[0] and et[i]/self.one_year_et+2000 <= interval[1]:
                    condition_not_streams = False
            if condition_not_streams:
                condition_23.append(velDust[i] > self._interstellar_min_vel and velDust[i] < self._interstellar_max_vel and mass[i] > self._interstellar_min_mass and mass[i] < self._interstellar_max_mass and self.data_without_999[i, indices['quality_flag_index']] > 1.5)
                condition_01.append(velDust[i] > self._interstellar_min_vel and velDust[i] < self._interstellar_max_vel and mass[i] > self._interstellar_min_mass and mass[i] < self._interstellar_max_mass and self.data_without_999[i, indices['quality_flag_index']] < 1.5)
            else:
                condition_23.append(False)
                condition_01.append(False)
            
        condition_23 = np.array(condition_23)
        condition_01 = np.array(condition_01)
        
        plt.figure()
        plt.hist(-detector_ISD_angles[condition_23]+180, label = '2&3', bins = 9, range = (0,180))
        plt.hist(-detector_ISD_angles[condition_01]+180, label = '0&1', alpha = 0.5, bins = 10, range = (0,180))
        plt.ylabel('Count')
        plt.xlabel('Angle between dust velocity and ISD [°]')
        plt.xlim(0,180)
        #plt.title('ISD min/max mass and min/max velocity and streams filtering')
        plt.legend()
        plt.savefig('wallImpacts.pdf')
        plt.show()
        
        
        plt.figure()
        plt.scatter(-detector_ISD_angles[condition_23]+180, velDust[condition_23], s = 1, label = '2&3')
        plt.scatter(-detector_ISD_angles[condition_01]+180, velDust[condition_01], s = 1, label = '0&1')
        plt.ylabel('Particle Velocity [km/s]')
        plt.xlabel('Angle between dust velocity and ISD [°]')
        plt.legend()
        plt.savefig('wallImpactsVel.pdf')
        plt.show()
        
        plt.figure()
        plt.scatter(-detector_ISD_angles[condition_23]+180, self.data_without_999[condition_23,indices['velocity_index']], s = 1, label = '2&3')
        plt.scatter(-detector_ISD_angles[condition_01]+180, self.data_without_999[condition_01,indices['velocity_index']], s = 1, label = '0&1')
        plt.ylabel('Particle Speed in Ulysses reference frame [km/s]')
        plt.xlabel('Angle between dust velocity and ISD [°]')
        plt.legend()
        plt.tight_layout()
        #plt.savefig('wallImpactsVel.pdf')
        plt.show()
        
        beta_data = []
        
        cndtn_nt_intrstllr = []
        
        streams_particles = []
        not_streams_particles = []
        ISD_particles = []
        
        for i, data in  enumerate(self.data_without_999):
            #A minimum requirement for beta particles is a certain minimum velocity and maximum detector_sun_angle
            condition_wehry = self._detector_sun_angles[i] <= self._wehry_angle and self._velocities[i] >= self._wehry_velocity
            
            #Particles are considered as jupiter steam particles if they were detected within Strub's jupiter stream times
            condition_not_streams = True
            for interval in self._tobias_streams:
                if et[i]/self.one_year_et+2000 >= interval[0] and et[i]/self.one_year_et+2000 <= interval[1]:
                    condition_not_streams = False
                    streams_particles.append(data)
                    
                    
            if condition_not_streams:
                not_streams_particles.append(data)
            
            #Particles are classified ISD if they come from a certain direction, have a certain minimum velocity and a certain minimum mass
            condition_interstellar_angle = detector_ISD_angles[i] - 180 > -self._tolerance
            condition_interstellar_vel = velDust[i] > self._interstellar_min_vel
            
            
            condition_interstellar_mass = mass[i] > self._interstellar_min_mass
            condition_interstellar_mass = True
            condition_not_interstellar = not (condition_interstellar_angle and condition_interstellar_vel and condition_interstellar_mass and condition_not_streams)
            
            #if i != 0:
            #    condition_not_interstellar = True
            
            if condition_not_streams and not condition_not_interstellar:
                ISD_particles.append(data)
            
            cndtn_nt_intrstllr.append(condition_not_interstellar)
            
            #Particles are considered beta if they pass the minimum requirement and are not ISD or jupiter stream particles
            #condition_not_interstellar = True
            
            condition_interesting = not i in self._interesting_indices
            condition_interesting = True
            condition = condition_wehry and condition_not_streams and condition_not_interstellar and condition_interesting
            if condition:
                beta_data.append(data)
                beta_meteoroids.append(True)
                beta_angles.append(self._detector_sun_angles[i])
                beta_vel.append(self._velocities[i])
            else:
                beta_meteoroids.append(False)
        
        self._streams_particles = np.array(streams_particles)
        self._not_streams_particles = np.array(not_streams_particles)
        self._ISD_particles = np.array(ISD_particles)
        self._condition_not_interstellar = cndtn_nt_intrstllr
        
        time_window = 1/10
        current_time = self._ISD_particles[0, self._time_index]/self.one_year_et+2000
        bin_times = []
        avg_mass = []
        mass_error = []
        while current_time < self._ISD_particles[-1, self._time_index]/self.one_year_et+2000:
            condition_0 = current_time < self._ISD_particles[:, self._time_index]/self.one_year_et+2000
            condition_1 = self._ISD_particles[:, self._time_index]/self.one_year_et+2000 < current_time+time_window
            condition = np.array([condition_0[i] and condition_1[i] for i in range(len(condition_0))])
            mass_points = np.log10(self._ISD_particles[condition,indices['mass_index']])
            if len(mass_points)>0:
                mean,error = wam(mass_points,np.log10(self._ISD_particles[condition,indices['mass_error_index']]))
                bin_times.append(current_time+time_window/2)
                avg_mass.append(mean)
                mass_error.append(error)
            current_time += time_window
        
        bin_times=np.array(bin_times)
        avg_mass=np.array(avg_mass)
        mass_error=np.array(mass_error)
        np.savetxt('ISDmass.dat', np.array([bin_times,avg_mass,mass_error]).T)
        print(len(self._ISD_particles))
        fig, ax1 = plt.subplots()
        ax1.errorbar(bin_times, avg_mass, yerr=mass_error, label = 'Binned every 0.1 years', ls = '', marker = 'x')
        ax1.set_xlabel('Year')
        ax1.set_ylabel(r'log$_{10}$(Mass [g])')
        ax1.legend()
        
        # Create a twin Axes for the second x-axis at the top
        ax2 = ax1.twiny()
        
        flyby = 1992.17646473
        aphelia = [1998.28180604, 2004.52656168]
        maxima = [1995+238/365.2425, 2001+303.5/365.2425]
        minima = [1994+281/365.2425, 2000+353/365.2425, 2007+69/365.2425]
        
        
        # Add specific times to the top x-axis
        specific_times = [flyby, minima[0], maxima[0], aphelia[0], minima[1], maxima[1], aphelia[1], minima[2]]  # Add the specific times you want to annotate
        specific_phrases = ['Jupiter Fly-By', 'South Pole', 'North Pole', 'Aphelion', 'South Pole', 'North Pole', 'Aphelion', 'South Pole']  # Corresponding phrases
        
        ax2.set_xticks(specific_times)
        ax2.set_xticklabels(specific_phrases, rotation=45, ha='left')  # Adjust rotation and ha
        
        ax2.set_xlim(ax1.get_xlim())
        
        plt.tight_layout()
        plt.savefig('InterstellarMassOverTime.pdf')
        plt.show()
        
        self._beta_meteoroids = beta_meteoroids
        self._beta_angles = beta_angles
        self._beta_vel = beta_vel
        
        self._beta_data = np.array(beta_data)
        
        plt.figure()
        plt.hist(np.log10(self.data_without_999[[not elem for elem in cndtn_nt_intrstllr], indices['mass_index']]), label = 'Interstellar', bins = 9, range = (-17,-8))
        plt.hist(np.log10(self._beta_data[:, indices['mass_index']]), label = 'Beta', bins = 9, range = (-17,-8))
        plt.xlabel('log(Mass [g])')
        plt.ylabel('Count')
        plt.yscale('log')
        plt.xlim(-17,-8)
        plt.legend()
        plt.savefig('massHist.pdf')
        plt.show()
        
        beta_angles = np.array(beta_angles)
        
        condition_23 = []
        condition_01 = []
        for i in range(len(beta_data)):
            condition_23.append(self._beta_data[i, indices['quality_flag_index']] > 1.5)
            condition_01.append(self._beta_data[i, indices['quality_flag_index']] < 1.5)
            
        condition_23 = np.array(condition_23)
        condition_01 = np.array(condition_01)
        
        # Define the bin edges
        bin_edges = np.array([0,5,10,15,20,25,30,35,40,45,50])  # Adjust the number of bins as needed
        
        # Create histograms for class 01 and class 23 impacts
        hist_23, _ = np.histogram(beta_angles[condition_23], bins=bin_edges)
        hist_01, _ = np.histogram(beta_angles[condition_01], bins=bin_edges)
        
        
        # Normalize the histograms to 1
        hist_23_old = hist_23
        hist_23 = np.array([hist_23[i] / (hist_23[i]+hist_01[i]) for i in range(len(hist_23))])
        hist_01 = np.array([hist_01[i] / (hist_23_old[i]+hist_01[i]) for i in range(len(hist_23))])
        
        # Plot the stacked histogram
        plt.bar(bin_edges[:-1]-2.5, hist_23, width=bin_edges[1] - bin_edges[0], label='2&3')
        plt.bar(bin_edges[:-1]-2.5, hist_01, width=bin_edges[1] - bin_edges[0], label='0&1', bottom=hist_23)
        
        plt.ylabel('Normalized Count')
        plt.xlabel('Angle between beta flux and sun in solar reference frame [°]')
        plt.legend()
        plt.xlim(0,50)
        plt.show()
        
        fig, ax1 = plt.subplots()
        print(len(self._beta_data), 'beta particles')
        ax1.scatter(self.data[:, self._time_index]/self.one_year_et+2000, self.data[:,indices['LAT']], label = 'all', alpha = 0.5, marker = '.', s= 1)
        ax1.plot(self._beta_data[:, self._time_index]/self.one_year_et+2000, np.zeros(len(self._beta_data[:, self._time_index])), color = 'red')
        ax1.scatter(self._beta_data[:, self._time_index]/self.one_year_et+2000, self._beta_data[:,indices['LAT']], label = 'Directly from South', marker = 'x')
        ax1.set_xlabel('Year')
        ax1.set_ylabel('Ulysses Ecliptic Latitude [°]')
        ax1.legend()
        
        # Create a twin Axes for the second x-axis at the top
        ax2 = ax1.twiny()
        
        flyby = 1992.17646473
        aphelia = [1998.28180604, 2004.52656168]
        maxima = [1995+238/365.2425, 2001+303.5/365.2425]
        minima = [1994+281/365.2425, 2000+353/365.2425, 2007+69/365.2425]
        
        
        # Add specific times to the top x-axis
        specific_times = [flyby, minima[0], maxima[0], aphelia[0], minima[1], maxima[1], aphelia[1], minima[2]]  # Add the specific times you want to annotate
        specific_phrases = ['Jupiter Fly-By', 'South Pole', 'North Pole', 'Aphelion', 'South Pole', 'North Pole', 'Aphelion', 'South Pole']  # Corresponding phrases
        
        ax2.set_xticks(specific_times)
        ax2.set_xticklabels(specific_phrases, rotation=45, ha='left')  # Adjust rotation and ha
        
        ax2.set_xlim(ax1.get_xlim())
        
        plt.tight_layout()
        plt.savefig('latWithInterstellar.pdf')
        plt.show()
        
        fig, ax1 = plt.subplots()
        ax1.scatter(self.data[:, self._time_index]/self.one_year_et+2000, self.data[:,indices['LAT']], label = 'all', alpha = 0.5, marker = '.', s= 1)
        ax1.plot(self._beta_data[:, self._time_index]/self.one_year_et+2000, np.zeros(len(self._beta_data[:, self._time_index])), color = 'red')
        ax1.scatter(self.data_only_999[:, self._time_index]/self.one_year_et+2000, self.data_only_999[:, indices['LAT']], label = 'ROT = 999', marker = 'x')
        ax1.set_xlabel('Year')
        ax1.set_ylabel('Ulysses Ecliptic Latitude [°]')
        ax1.legend()
        
        # Create a twin Axes for the second x-axis at the top
        ax2 = ax1.twiny()
        
        flyby = 1992.17646473
        aphelia = [1998.28180604, 2004.52656168]
        maxima = [1995+238/365.2425, 2001+303.5/365.2425]
        minima = [1994+281/365.2425, 2000+353/365.2425, 2007+69/365.2425]
        
        
        # Add specific times to the top x-axis
        specific_times = [flyby, minima[0], maxima[0], aphelia[0], minima[1], maxima[1], aphelia[1], minima[2]]  # Add the specific times you want to annotate
        specific_phrases = ['Jupiter Fly-By', 'South Pole', 'North Pole', 'Aphelion', 'South Pole', 'North Pole', 'Aphelion', 'South Pole']  # Corresponding phrases
        
        ax2.set_xticks(specific_times)
        ax2.set_xticklabels(specific_phrases, rotation=45, ha='left')  # Adjust rotation and ha
        
        ax2.set_xlim(ax1.get_xlim())
        
        plt.tight_layout()
        plt.savefig('only999.pdf')
        plt.show()
        
        plt.xlabel('Solar Distance [au]')
        plt.ylabel('Particle Velocity [km/s]')
        plt.scatter(self._beta_data[:,self._dist_index], self._beta_vel)
        plt.savefig('VelDist.pdf')
        plt.show()
        

    def _compare_ISD_and_beta(self):
        ISD = np.loadtxt('/home/steint/Documents/sens_isd_2d_fixed.txt', delimiter = ',')
        ISD = np.loadtxt('/home/steint/Documents/sens_isd26/sens_isd_26km_apex_pm0_fixed.txt', delimiter = ',')
        
        beta = np.loadtxt('/home/steint/Documents/sens_wehry_2d_fixed.txt', delimiter = ',')
        
        ISD_time = []
        ISD_rest = []
        for line in ISD[::10]:
            ISD_time.append(line[0])
            ISD_rest.append(line[1:])
        
        beta_time = []
        beta_rest = []
        for line in beta[::10]:
            beta_time.append(line[0])
            beta_rest.append(line[1:])
        
        ISD_rest = np.array(ISD_rest)
        beta_rest = np.array(beta_rest)
        
        beta_impact_time = self._beta_data[:,self._time_index]
        beta_impact_rot = self._beta_data[:,indices['rotation_angle_index']]
        
        ISD_impact_time = self._ISD_particles[:,self._time_index]
        ISD_impact_rot = self._ISD_particles[:,indices['rotation_angle_index']]
        
        # Create a new figure with subplots and adjusted height ratios
        fig, ax1 = plt.subplots(1, 1, figsize=(6, 8))
        
        maxEffAreaValue = find_max_nested_structure([ISD_rest, beta_rest, np.sqrt(ISD_rest*beta_rest)])
        print(maxEffAreaValue)
        maxEffAreaValue = 1386.1216
        
        # Plot the first image (ISD_rest) in the first subplot
        im = ax1.imshow(ISD_rest, vmin = 0, vmax = maxEffAreaValue)
        ax1.set_title('ISD')
        
        
        # Set custom y-axis ticks using the 'time_selected' array, displaying only every 100th y-tick
        y_ticks_selected = ISD_time[::100]
        ax1.set_yticks(np.arange(0, len(ISD_time), 100))
        ax1.set_yticklabels(["{:.2f}".format(label) for label in y_ticks_selected])
        
        # Add color bar
        cbar = plt.colorbar(im, ax=ax1, orientation='horizontal')
        cbar.set_label(label=r'Effective area [cm$^2$]')
        
        ax1.set_xlabel('Rotation angle [°]')
        ax1.set_ylabel('Year')
        
        # Show the combined figure with both images
        plt.savefig('directional_pm0.pdf')
        plt.show()
        
        fig, (ax1, ax2, ax3, ax4, ax5, ax6) = plt.subplots(1, 6, figsize=(20, 8))
        
        # Plot the first image (ISD_rest) in the first subplot
        im = ax1.imshow(ISD_rest, vmin = 0, vmax = maxEffAreaValue)
        ax1.set_title('ISD')
        
        # Plot the second image (beta_rest) in the second subplot
        ax2.imshow(beta_rest, vmin = 0, vmax = maxEffAreaValue)
        ax2.set_title('beta')
        
        # Set custom y-axis ticks using the 'time_selected' array, displaying only every 100th y-tick
        y_ticks_selected = ISD_time[::100]
        ax1.set_yticks(np.arange(0, len(ISD_time), 100))
        ax1.set_yticklabels(["{:.2f}".format(label) for label in y_ticks_selected])
        ax2.set_yticks(np.arange(0, len(ISD_time), 100))
        ax2.set_yticklabels(["{:.2f}".format(label) for label in y_ticks_selected])
        
        ax3.imshow(np.sqrt(ISD_rest*beta_rest), vmin = 0, vmax = maxEffAreaValue)
        ax3.set_title('sqrt(ISD*beta)')
        
        ax3.set_yticks(np.arange(0, len(ISD_time), 100))
        ax3.set_yticklabels(["{:.2f}".format(label) for label in y_ticks_selected])
        
        alpha = 0.9
        
        # Calculate the corresponding x positions for the scatter plot based on 'rot' values
        ax4.scatter(beta_impact_rot, beta_impact_time/self.one_year_et+2000, marker = 'x', color = 'red', alpha = alpha)
        ax4.set_ylim([ISD_time[0], ISD_time[-1]])
        ax4.invert_yaxis()
        
        ax5.scatter(beta_impact_rot, beta_impact_time/self.one_year_et+2000, marker = 'x', color = 'red', alpha = alpha)
        ax5.set_ylim([ISD_time[0], ISD_time[-1]])
        ax5.invert_yaxis()
        
        ax6.scatter(beta_impact_rot, beta_impact_time/self.one_year_et+2000, marker = 'x', color = 'red', alpha = alpha)
        ax6.set_ylim([ISD_time[0], ISD_time[-1]])
        ax6.invert_yaxis()
        
        # Adjust the height of ax1, ax2, and ax3 individually
        ax1.set_position([0.125, 0.1, 0.14, 1])  # Adjust the position and height of ax1
        ax4.set_position([0.125, 0.295, 0.14, 0.606])  # Adjust the position and height of ax4
        
        ax2.set_position([0.33, 0.1, 0.14, 1])  # Adjust the position and height of ax2
        ax5.set_position([0.33, 0.295, 0.14, 0.606])  # Adjust the position and height of ax5
        
        ax3.set_position([0.53, 0.1, 0.14, 1])  # Adjust the position and height of ax3
        ax6.set_position([0.53, 0.295, 0.14, 0.606])  # Adjust the position and height of ax6
        
        ax4.set_facecolor('none')
        ax5.set_facecolor('none')
        ax6.set_facecolor('none')
        
        # Remove y-axis and x-axis labels
        ax1.set_yticks([])
        ax1.set_xticks([])
        ax2.set_yticks([])
        ax2.set_xticks([])
        ax3.set_yticks([])
        ax3.set_xticks([])
    
        # Show the combined figure with both images
        plt.savefig('directional_eff_area_beta_26_pm0.pdf')
        plt.show()
        
        # Create a new figure with subplots and adjusted height ratios
        fig, (ax1, ax2, ax3, ax4, ax5, ax6) = plt.subplots(1, 6, figsize=(20, 8))
        
        maxEffAreaValue = find_max_nested_structure([ISD_rest, beta_rest, np.sqrt(ISD_rest*beta_rest)])
        
        # Plot the first image (ISD_rest) in the first subplot
        im = ax1.imshow(ISD_rest, vmin = 0, vmax = maxEffAreaValue)
        ax1.set_title('ISD')
        
        # Plot the second image (beta_rest) in the second subplot
        ax2.imshow(beta_rest, vmin = 0, vmax = maxEffAreaValue)
        ax2.set_title('beta')
        
        # Set custom y-axis ticks using the 'time_selected' array, displaying only every 100th y-tick
        y_ticks_selected = ISD_time[::100]
        ax1.set_yticks(np.arange(0, len(ISD_time), 100))
        ax1.set_yticklabels(["{:.2f}".format(label) for label in y_ticks_selected])
        ax2.set_yticks(np.arange(0, len(ISD_time), 100))
        ax2.set_yticklabels(["{:.2f}".format(label) for label in y_ticks_selected])
        
        ax3.imshow(np.sqrt(ISD_rest*beta_rest), vmin = 0, vmax = maxEffAreaValue)
        ax3.set_title('sqrt(ISD*beta)')
        
        ax3.set_yticks(np.arange(0, len(ISD_time), 100))
        ax3.set_yticklabels(["{:.2f}".format(label) for label in y_ticks_selected])
        
        # Calculate the corresponding x positions for the scatter plot based on 'rot' values
        ax4.scatter(ISD_impact_rot, ISD_impact_time/self.one_year_et+2000, marker = 'x', color = 'red', alpha = alpha)
        ax4.set_ylim([ISD_time[0], ISD_time[-1]])
        ax4.invert_yaxis()
        
        ax5.scatter(ISD_impact_rot, ISD_impact_time/self.one_year_et+2000, marker = 'x', color = 'red', alpha = alpha)
        ax5.set_ylim([ISD_time[0], ISD_time[-1]])
        ax5.invert_yaxis()
        
        ax6.scatter(ISD_impact_rot, ISD_impact_time/self.one_year_et+2000, marker = 'x', color = 'red', alpha = alpha)
        ax6.set_ylim([ISD_time[0], ISD_time[-1]])
        ax6.invert_yaxis()
        
        # Adjust the height of ax1, ax2, and ax3 individually
        ax1.set_position([0.125, 0.1, 0.14, 1])  # Adjust the position and height of ax1
        ax4.set_position([0.125, 0.295, 0.14, 0.606])  # Adjust the position and height of ax4
        
        ax2.set_position([0.33, 0.1, 0.14, 1])  # Adjust the position and height of ax2
        ax5.set_position([0.33, 0.295, 0.14, 0.606])  # Adjust the position and height of ax5
        
        ax3.set_position([0.53, 0.1, 0.14, 1])  # Adjust the position and height of ax3
        ax6.set_position([0.53, 0.295, 0.14, 0.606])  # Adjust the position and height of ax6
        
        ax4.set_facecolor('none')
        ax5.set_facecolor('none')
        ax6.set_facecolor('none')
        
        # Remove y-axis and x-axis labels
        ax1.set_yticks([])
        ax1.set_xticks([])
        ax2.set_yticks([])
        ax2.set_xticks([])
        ax3.set_yticks([])
        ax3.set_xticks([])
        
        # Show the combined figure with both images
        plt.savefig('directional_eff_area_ISD_26_pm0.pdf')
        plt.show()
        
        eff_area_array = []
        for i, time in enumerate(ISD_impact_time):
            idx = find_nearest_idx(ISD_time, time/self.one_year_et+2000)
            eff_area_array.append(ISD_rest[idx, int(ISD_impact_rot[i])])
        eff_area_array = np.array(eff_area_array)
        #UNITS UNITS UNITS
        flux = []
        
        time_window = 1/12
        
        
        combined_array = np.array([np.concatenate((ISD_impact_time, beta_impact_time)), np.concatenate((ISD_impact_rot, beta_impact_rot))])
        # Get the indices that would sort the first row (time array)
        sorted_indices = np.argsort(combined_array[0])
        
        # Apply the sorted indices to both arrays
        combined_array = combined_array[:, sorted_indices]
        combined_time = combined_array[0]
        combined_rot = combined_array[1]
        
        
        current_time = combined_time[0]/self.one_year_et+2000
        avg_eff_area_array = []
        count = []
        while current_time < combined_time[-1]/self.one_year_et+2000:
            condition_0 = current_time < ISD_impact_time/self.one_year_et+2000
            condition_1 = ISD_impact_time/self.one_year_et+2000 < current_time+time_window
            condition = np.array([condition_0[i] and condition_1[i] for i in range(len(condition_0))])
            eff_area_points = eff_area_array[condition]
            avg_eff_area = np.mean(eff_area_points)
            avg_eff_area /= 10000
            if len(eff_area_points)/avg_eff_area/time_window/self.one_year_et == np.inf:
                    count.append(0)
                    flux.append(0)
                    avg_eff_area_array.append(0)
            else:
                count.append(len(eff_area_points))
                if len(eff_area_points) == 0:
                    flux.append(0)
                    avg_eff_area_array.append(0)
                else:
                    avg_eff_area_array.append(avg_eff_area)
                    flux.append(len(eff_area_points)/avg_eff_area/time_window/self.one_year_et)
            current_time += time_window
        flux_times = np.linspace(ISD_impact_time[0]/self.one_year_et+2000+time_window/2,current_time-time_window/2,len(flux))
        
        fig, ax1 = plt.subplots()
        
        ax1.scatter(flux_times, flux, label = 'ISD', marker = 'x', alpha = 0.9)
        
        #ax1.set_ylim(0,0.001)
            
        flux = np.array(flux)
        count = np.array(count)
        meanFlux = np.mean(flux)
        print(meanFlux)
        avg_eff_area_array = np.array(avg_eff_area_array)
        old_Lambda = np.array([int(meanFlux*avg_eff_area_array[i]*time_window*self.one_year_et+0.5) for i in range(len(flux))])
        poissonCDF = poisson.cdf(count-1,old_Lambda)
        
        old_ISD_y = 1-poissonCDF
        
        non_outliers = old_ISD_y > 0.01
        meanFlux = np.mean(flux[non_outliers])
        print(meanFlux)
        avg_eff_area_array = np.array(avg_eff_area_array)
        Lambda = np.array([int(meanFlux*avg_eff_area_array[i]*time_window*self.one_year_et+0.5) for i in range(len(flux))])
        poissonCDF = poisson.cdf(count-1,Lambda)
        
        ISD_flux_times = flux_times
        ISD_y = 1-poissonCDF

        
        eff_area_array = []
        for i, time in enumerate(combined_time):
            idx = find_nearest_idx(ISD_time, time/self.one_year_et+2000)
            eff_area_array.append(ISD_rest[idx, int(combined_rot[i])])
        eff_area_array = np.array(eff_area_array)
        flux = []
        
        current_time = combined_time[0]/self.one_year_et+2000
        #avg_eff_area_array = []
        count = []
        while current_time < combined_time[-1]/self.one_year_et+2000:
            condition_0 = current_time < combined_time/self.one_year_et+2000
            condition_1 = combined_time/self.one_year_et+2000 < current_time+time_window
            condition = np.array([condition_0[i] and condition_1[i] for i in range(len(condition_0))])
            eff_area_points = eff_area_array[condition]
            avg_eff_area = np.mean(eff_area_points)
            avg_eff_area /= 10000
            if len(eff_area_points)/avg_eff_area/time_window/self.one_year_et == np.inf:
                    count.append(0)
                    flux.append(0)
                    #avg_eff_area_array.append(0)
            else:
                count.append(len(eff_area_points))
                if len(eff_area_points) == 0:
                    flux.append(0)
                    #avg_eff_area_array.append(0)
                else:
                    #avg_eff_area_array.append(avg_eff_area)
                    flux.append(len(eff_area_points)/avg_eff_area/time_window/self.one_year_et)
            current_time += time_window
        flux_times = np.linspace(combined_time[0]/self.one_year_et+2000+time_window/2,current_time-time_window/2,len(flux))
        ax1.scatter(flux_times, flux, label = 'Combined', marker = '+', alpha = 0.9)
        
        
        #flux = np.array(flux)
        count = np.array(count)
        #meanFlux = np.mean(flux)
        #print(meanFlux)
        #avg_eff_area_array = np.array(avg_eff_area_array)
        #Lambda = np.array([int(meanFlux*avg_eff_area_array[i]*time_window*self.one_year_et+0.5) for i in range(len(flux))])
        poissonCDF = poisson.cdf(count-1,Lambda)
        
        combined_flux_times = flux_times
        combined_y = 1-poissonCDF
        
        
        ax1.set_xlabel('Year')
        ax1.set_ylabel(r'Flux [1/(m$^2$s)]')
        
        # Create a twin Axes for the second x-axis at the top
        ax2 = ax1.twiny()
        
        flyby = 1992.17646473
        aphelia = [1998.28180604, 2004.52656168]
        maxima = [1995+238/365.2425, 2001+303.5/365.2425]
        minima = [1994+281/365.2425, 2000+353/365.2425, 2007+69/365.2425]
        
        
        # Add specific times to the top x-axis
        specific_times = [flyby, minima[0], maxima[0], aphelia[0], minima[1], maxima[1], aphelia[1], minima[2]]  # Add the specific times you want to annotate
        specific_phrases = ['Jupiter Fly-By', 'South Pole', 'North Pole', 'Aphelion', 'South Pole', 'North Pole', 'Aphelion', 'South Pole']  # Corresponding phrases
        
        ax2.set_xticks(specific_times)
        ax2.set_xticklabels(specific_phrases, rotation=45, ha='left')  # Adjust rotation and ha
        
        ax2.set_xlim(ax1.get_xlim())
        
        ax1.legend()
        plt.tight_layout()
        plt.savefig('flux_26_pm0.pdf')
        plt.show()
        
        fig, ax1 = plt.subplots()
        
        ax1.set_xlabel('Year')
        ax1.set_ylabel(r'p(Flux $\geq$ Measured Flux)')
        ax1.scatter(ISD_flux_times, ISD_y, marker = 'x', alpha = 0.9, label = 'ISD')
        ax1.scatter(combined_flux_times, combined_y, marker = '+', alpha = 0.9, label = 'Combined')
        ax1.legend()
        ax1.set_yscale('log')
        plt.grid()
        
        
        # Create a twin Axes for the second x-axis at the top
        ax2 = ax1.twiny()
        
        flyby = 1992.17646473
        aphelia = [1998.28180604, 2004.52656168]
        maxima = [1995+238/365.2425, 2001+303.5/365.2425]
        minima = [1994+281/365.2425, 2000+353/365.2425, 2007+69/365.2425]
        
        
        # Add specific times to the top x-axis
        specific_times = [flyby, minima[0], maxima[0], aphelia[0], minima[1], maxima[1], aphelia[1], minima[2]]  # Add the specific times you want to annotate
        specific_phrases = ['Jupiter Fly-By', 'South Pole', 'North Pole', 'Aphelion', 'South Pole', 'North Pole', 'Aphelion', 'South Pole']  # Corresponding phrases
        
        ax2.set_xticks(specific_times)
        ax2.set_xticklabels(specific_phrases, rotation=45, ha='left')  # Adjust rotation and ha
        
        ax2.set_xlim(ax1.get_xlim())
        
        plt.tight_layout()
        plt.savefig('poisson_26_pm0.pdf')
        
        plt.show()

    def _effective_area(self) -> (list, list):
        eff_area_data = np.loadtxt(self.eff_area_file, delimiter = ',')
        if self.eff_area_file == 'DefaultDataset.csv':
            eff_area_data[:,1] = eff_area_data[:,1]*10000
        eff_area_time = []
        for i in eff_area_data[:,0]:        
            eff_area_time.append(spice.str2et(str(i)[:4]+'-01-001T00:00')+float(str(i)[4:])*self.one_year_et)
        eff_area_time = np.array(eff_area_time)
        self._eff_area_data = eff_area_data
        self.eff_area_time = eff_area_time

    def _minimum_effective_area_hist(self):
        
        GM_km3_per_s2 = 1.327e11
        au_in_km  = 149597870.7
        
        peri = []
        ecc = []
        lnode = []
        lnode_time = []
        self._write_new_data()
        beta_values = []
        
        beta_data = np.loadtxt('silicate0.00.txt', usecols = (1,2), skiprows = 1, delimiter = ' ')
        beta_radii = np.loadtxt('silicate0.00.txt', usecols = (0), skiprows = 1, delimiter = ' ')
        mass = self._new_data[:,indices['mass_index']]/1000
        
        lon = self._new_data[:, indices['solar_lon_index']]
        lat = self._new_data[:, indices['solar_lat_index']]
        for i in range(len(self._new_data)):
            idx = find_nearest_idx(beta_data[:,0], mass[i])
            [state, ltime] = spice.spkezr('ULYSSES',  self._new_data[i,self._time_index],      'ECLIPJ2000', 'NONE', 'SUN')
            beta = beta_data[idx,1]
            
            posUlysses = state[:3]
            velUlysses = state[3:]
            velImpact = self._new_data[i, indices['velocity_index']]
            pointingDetector = spice.latrec(1, lon[i]*2*np.pi/360, lat[i]*2*np.pi/360)
            vel = velUlysses-velImpact*pointingDetector
            
            elts = spice.oscelt(np.array([posUlysses[0], posUlysses[1], posUlysses[2], vel[0], vel[1], vel[2]]), self._new_data[i,self._time_index], (1-beta)*GM_km3_per_s2)
            peri.append(elts[0]/au_in_km)
            ecc.append(elts[1])
            beta_values.append(beta)
            if self._new_data[i,indices['LAT']] > 5:
                lnode.append(elts[3]/np.pi*180)
                lnode_time.append(self._new_data[i,self._time_index]/self.one_year_et+2000)
        
        
        print('mean beta', np.mean(np.array(beta_values)))
        
        beta_plot = []
        sol_dist_plot = []
        beta_low = []
        beta_high = []
        for i in range(len(self._new_data)):
            idx = find_nearest_idx(beta_data[:,0], mass[i])
            beta = beta_data[idx,1]
            beta_plot.append(beta)
            sol_dist_plot.append(self._new_data[i,self._dist_index])
            
            beta_temp = beta
            
            idx = find_nearest_idx(beta_data[:,0], mass[i]/self._new_data[i,indices['mass_error_index']])
            beta1 = beta_data[idx,1]
            idx = find_nearest_idx(beta_data[:,0], mass[i]*self._new_data[i,indices['mass_error_index']])
            beta2 = beta_data[idx,1]
            if beta1 < beta2:
                beta_low.append(np.abs(beta1-beta_temp))
                beta_high.append(np.abs(beta2-beta_temp))
            else:
                beta_low.append(np.abs(beta2-beta_temp))
                beta_high.append(np.abs(beta1-beta_temp))
        
        peri_low = []
        peri_high = []
        ecc_low = []
        ecc_high = []
        for i in range(len(self._new_data)):
            idx = find_nearest_idx(beta_data[:,0], mass[i])
            [state, ltime] = spice.spkezr('ULYSSES',  self._new_data[i,self._time_index],      'ECLIPJ2000', 'NONE', 'SUN')
            
            posUlysses = state[:3]
            velUlysses = state[3:]
            velImpact = self._new_data[i, indices['velocity_index']]
            pointingDetector = spice.latrec(1, lon[i]*2*np.pi/360, lat[i]*2*np.pi/360)
            vel = velUlysses-velImpact*pointingDetector
            
            elts = spice.oscelt(np.array([posUlysses[0], posUlysses[1], posUlysses[2], vel[0], vel[1], vel[2]]), self._new_data[i,self._time_index], (1-beta_low[i])*GM_km3_per_s2)
            peri_low.append(elts[0]/au_in_km)
            ecc_low.append(elts[1])
            
            if beta_high[i] <= 1:
                elts = spice.oscelt(np.array([posUlysses[0], posUlysses[1], posUlysses[2], vel[0], vel[1], vel[2]]), self._new_data[i,self._time_index], (1-beta_high[i])*GM_km3_per_s2)
            else:
                elts = spice.oscelt(np.array([posUlysses[0], posUlysses[1], posUlysses[2], vel[0], vel[1], vel[2]]), self._new_data[i,self._time_index], 0.000001*GM_km3_per_s2)
            peri_high.append(elts[0]/au_in_km)
            ecc_high.append(elts[1])
           
            
        fig, ax1 = plt.subplots()
        
        ax1.plot(beta_data[:,0], beta_data[:,1])
        ax1.set_xlabel('Particle mass [kg]')
        ax1.set_ylabel(r'$\beta$')
        ax1.set_xscale('log')
        ax1.set_yscale('log')

        # Create a twin Axes for the second x-axis at the top
        ax2 = ax1.twiny()
        
        ax2.plot(beta_radii, beta_data[:,1])
        ax2.set_xlabel(r'Particle radius [$\mu$m]')
        ax2.set_xscale('log')
        ax2.set_yscale('log')
        plt.tight_layout()
        
        plt.savefig('betacurve.pdf')
        plt.show()
        
        
        plt.figure()
        plt.errorbar(sol_dist_plot, beta_plot, yerr = [beta_low, beta_high], ls = '', marker = 'x')
        plt.ylim(bottom=0, top = max(beta_data[:,1]))
        plt.xlabel('Ulysses Solar Distance [au]')
        plt.ylabel(r'$\beta$')
        plt.savefig('betavalues.pdf')
        plt.show()
        
        plt.figure()
        plt.scatter(sol_dist_plot, self._new_data[:,indices['LAT']], marker = 'x')
        plt.xlabel('Ulysses Solar Distance [au]')
        plt.ylabel('Ulysses Ecliptic Latitude [°]')
        #plt.savefig('latvsdist.pdf')
        plt.show()
        
        plt.figure()
        plt.xlabel('Perihelion distance [au]')
        plt.ylabel('Count')
        plt.hist(peri_low, label = r'Low $\beta$', bins = 6, range = (0,3))
        plt.legend()
        plt.xlim(0,3)
        plt.savefig('periLow.pdf')
        plt.show()
        
        plt.figure()
        plt.xlabel('Perihelion distance [au]')
        plt.ylabel('Count')
        plt.hist(peri, label = r'Expected $\beta$', bins = 6, range = (0,3))
        #plt.legend()
        plt.xlim(0,3)
        plt.savefig('periExpected.pdf')
        plt.show()
        
        plt.figure()
        plt.xlabel('Longitude of the ascending node [°]')
        plt.ylabel('Count')
        plt.hist(lnode, bins = 12, range = (0,360))
        plt.xticks(ticks = [0,30,60,90,120,150,180,210,240,270,300,330,360])
        plt.savefig('lnode.pdf')
        plt.xlim(0,360)
        plt.show()
        
        fig, ax1 = plt.subplots()
        ax1.set_xlabel('Year')
        ax1.scatter(lnode_time,lnode)
        
        # Create a twin Axes for the second x-axis at the top
        ax2 = ax1.twiny()
        
        flyby = 1992.17646473
        aphelia = [1998.28180604, 2004.52656168]
        maxima = [1995+238/365.2425, 2001+303.5/365.2425]
        minima = [1994+281/365.2425, 2000+353/365.2425, 2007+69/365.2425]
        
        
        # Add specific times to the top x-axis
        specific_times = [flyby, minima[0], maxima[0], aphelia[0], minima[1], maxima[1], aphelia[1], minima[2]]  # Add the specific times you want to annotate
        specific_phrases = ['Jupiter Fly-By', 'South Pole', 'North Pole', 'Aphelion', 'South Pole', 'North Pole', 'Aphelion', 'South Pole']  # Corresponding phrases
        
        ax2.set_xticks(specific_times)
        ax2.set_xticklabels(specific_phrases, rotation=45, ha='left')  # Adjust rotation and ha
        
        ax2.set_xlim(ax1.get_xlim())
        
        ax1.set_ylabel('Longitude of the ascending node [°]')
        plt.tight_layout()
        plt.savefig('lnodeTime.pdf')
        plt.show()
        
        plt.figure()
        plt.xlabel('Perihelion distance [au]')
        plt.ylabel('Count')
        plt.hist(peri_high, label = r'High $\beta$', bins = 6, range = (0,3))
        plt.legend()
        plt.xlim(0,3)
        plt.savefig('periHigh.pdf')
        plt.show()
        
        plt.figure()
        plt.xlabel('Eccentricity')
        plt.ylabel('Count')
        plt.hist(ecc_low, label = r'Low $\beta$', bins = 16, range = (0,16))
        plt.legend()
        plt.xlim(0,16)
        plt.savefig('eccLow.pdf')
        plt.show()
        
        plt.figure()
        plt.xlabel('Eccentricity')
        plt.ylabel('Count')
        plt.hist(ecc, label = r'Expected $\beta$', bins = 16, range = (0,16))
        print(ecc)
        #plt.legend()
        plt.xlim(0,16)
        plt.savefig('eccExpected.pdf')
        plt.show()
        
        plt.figure()
        plt.xlabel('Eccentricity')
        plt.ylabel('Count')
        plt.hist(ecc_high, label = r'High $\beta$', bins = 16, range = (0,16))
        plt.legend()
        plt.xlim(0,16)
        plt.savefig('eccHigh.pdf')
        plt.show()

    def _write_new_data(self):
        new_data = []
        beta_dist = []
        eff_area_res = []
        
        for i in range(len(self.data_without_999)):
            if self._beta_meteoroids[i]:
                closest_eff_area_idx = find_nearest_idx(self.eff_area_time, self.data_without_999[i, self._time_index])
                eff_area_res.append(self._eff_area_data[closest_eff_area_idx,1])
                new_data.append(self.data_without_999[i])
                beta_dist.append(self.data_without_999[i,self._dist_index])
            
        if new_data == []:
            raise RuntimeError('No beta meteoroids found')
        
        beta_dist = np.array(beta_dist)
        new_data = np.array(new_data)
        eff_area_res = np.array(eff_area_res)
        self._new_data = new_data
        self._beta_dist = beta_dist
        self._eff_area_res = eff_area_res
        
    def _set_new_angles_and_velocities(self):
        self._new_detector_sun_angles, self._new_velocities = self._calculate_angle_and_velocities_between_two_objects(self._new_data)
        
    def _compare_found_betas(self):
        print('Found beta indices:', self._new_data[:, self._index_index])
        print(len(self._new_data[:,0]),'beta particles found')
        print('Wehry beta indices:', self._wehry_beta_particle_indices)
        print(len(self._wehry_beta_particle_indices),'beta particles found')
        my_indices = np.array(self._new_data[:, self._index_index])
        wehry_indices = np.array(self._wehry_beta_particle_indices)
        intersect_indices = np.intersect1d(my_indices, wehry_indices)
        wehry_not_me_indices = np.setdiff1d(wehry_indices, my_indices)
        print('Intersection beta indices:', intersect_indices)
        print(len(intersect_indices),'beta particles found')
        self._intersect_indices = intersect_indices
        self._wehry_not_me_indices = wehry_not_me_indices

    def _plot_lat(self):
        fig, ax1 = plt.subplots()
        ax1.scatter(self.data[:, self._time_index]/self.one_year_et+2000, self.data[:,indices['LAT']], label = 'all', alpha = 0.5, marker = '.', s= 1)
        #ax1.scatter(self.data[[int(self._wehry_not_me_indices[i]) for i in range(len(self._wehry_not_me_indices))], self._time_index]/self.one_year_et+2000, self.data[[int(self._wehry_not_me_indices[i]) for i in range(len(self._wehry_not_me_indices))],indices['LAT']], label = 'wehry_not_me', marker = 'x')
        ax1.plot(self._new_data[:, self._time_index]/self.one_year_et+2000, np.zeros(len(self._new_data[:, self._time_index])), color = 'red')
        ax1.scatter(self._new_data[:, self._time_index]/self.one_year_et+2000, self._new_data[:,indices['LAT']], label = 'beta', marker = 'x')
        
        southern = np.where(self._new_data[:,indices['LAT']] < 0)
        late = np.where(self._new_data[:, self._time_index]/self.one_year_et+2000 > 1993)
        far_away = np.where(self._new_data[:, self._dist_index] > 3)
        southern_late = np.intersect1d(southern,late)
        far_late = np.intersect1d(far_away,late)
        ax1.scatter(self._new_data[far_late, self._time_index]/self.one_year_et+2000, self._new_data[far_late,indices['LAT']], label = 'outer Solar System beta', marker = 's')
        ax1.scatter(self._new_data[southern_late, self._time_index]/self.one_year_et+2000, self._new_data[southern_late,indices['LAT']], label = 'southern beta', marker = 's')
        
        ax1.set_xlabel('Year')
        ax1.set_ylabel('Ulysses Ecliptic Latitude [°]')
        ax1.legend(loc='lower left')
        
        # Create a twin Axes for the second x-axis at the top
        ax2 = ax1.twiny()
        
        flyby = 1992.17646473
        aphelia = [1998.28180604, 2004.52656168]
        maxima = [1995+238/365.2425, 2001+303.5/365.2425]
        minima = [1994+281/365.2425, 2000+353/365.2425, 2007+69/365.2425]
        
        
        # Add specific times to the top x-axis
        specific_times = [flyby, minima[0], maxima[0], aphelia[0], minima[1], maxima[1], aphelia[1], minima[2]]  # Add the specific times you want to annotate
        specific_phrases = ['Jupiter Fly-By', 'South Pole', 'North Pole', 'Aphelion', 'South Pole', 'North Pole', 'Aphelion', 'South Pole']  # Corresponding phrases
        
        ax2.set_xticks(specific_times)
        ax2.set_xticklabels(specific_phrases, rotation=45, ha='left')  # Adjust rotation and ha
        
        ax2.set_xlim(ax1.get_xlim())
        
        plt.tight_layout()
        plt.savefig('lat.pdf')
        plt.show()
        
        fig, ax1 = plt.subplots()
        ax1.scatter(self.data[:, self._time_index]/self.one_year_et+2000, self.data[:,indices['LAT']], label = 'all', alpha = 0.5, marker = '.', s= 1)
        #ax1.scatter(self.data[[int(self._wehry_not_me_indices[i]) for i in range(len(self._wehry_not_me_indices))], self._time_index]/self.one_year_et+2000, self.data[[int(self._wehry_not_me_indices[i]) for i in range(len(self._wehry_not_me_indices))],indices['LAT']], label = 'wehry_not_me', marker = 'x')
        ax1.plot(self._new_data[:, self._time_index]/self.one_year_et+2000, np.zeros(len(self._new_data[:, self._time_index])), color = 'red')
        ax1.scatter(self._new_data[:, self._time_index]/self.one_year_et+2000, self._new_data[:,indices['LAT']], label = 'beta', marker = 'x')
        ax1.set_xlabel('Year')
        ax1.set_ylabel('Ulysses Ecliptic Latitude [°]')
        
        ax1.plot(self.data[:, self._time_index]/self.one_year_et+2000, 75*np.sin((self.data[:, self._time_index]/self.one_year_et+2000-1980)/22*2*np.pi), color = 'black', label = 'Solar Cycle')
        
        southern = np.where(self._new_data[:,indices['LAT']] < 0)
        late = np.where(self._new_data[:, self._time_index]/self.one_year_et+2000 > 1993)
        far_away = np.where(self._new_data[:, self._dist_index] > 3)
        southern_late = np.intersect1d(southern,late)
        far_late = np.intersect1d(far_away,late)
        ax1.scatter(self._new_data[far_late, self._time_index]/self.one_year_et+2000, self._new_data[far_late,indices['LAT']], label = 'outer Solar System beta', marker = 's')
        ax1.scatter(self._new_data[southern_late, self._time_index]/self.one_year_et+2000, self._new_data[southern_late,indices['LAT']], label = 'southern beta', marker = 's')
        
        ax1.legend(loc='lower left')
        
        # Create a twin Axes for the second x-axis at the top
        ax2 = ax1.twiny()
        
        flyby = 1992.17646473
        aphelia = [1998.28180604, 2004.52656168]
        maxima = [1995+238/365.2425, 2001+303.5/365.2425]
        minima = [1994+281/365.2425, 2000+353/365.2425, 2007+69/365.2425]
        
        
        # Add specific times to the top x-axis
        specific_times = [flyby, minima[0], maxima[0], aphelia[0], minima[1], maxima[1], aphelia[1], minima[2]]  # Add the specific times you want to annotate
        specific_phrases = ['Jupiter Fly-By', 'South Pole', 'North Pole', 'Aphelion', 'South Pole', 'North Pole', 'Aphelion', 'South Pole']  # Corresponding phrases
        
        ax2.set_xticks(specific_times)
        ax2.set_xticklabels(specific_phrases, rotation=45, ha='left')  # Adjust rotation and ha
        
        ax2.set_xlim(ax1.get_xlim())
        
        # Create a twin Axes for the second x-axis at the top
        ax3 = ax1.twinx()
        
        
        # Add specific times to the top x-axis
        specific_values = [-75, 0, 75]  # Add the specific times you want to annotate
        specific_yphrases = ['Defocusing', 'Nominal Flux', 'Focusing']  # Corresponding phrases
        
        ax3.set_yticks(specific_values)
        #ax3.set_yticklabels(specific_yphrases, rotation=45, ha='left')  # Adjust rotation and ha
        ax3.set_yticklabels(specific_yphrases, ha='left')
        
        ax3.set_ylim(ax1.get_ylim())
        
        plt.tight_layout()
        
        
        
        plt.savefig('solarCycle.pdf')
        plt.show()
        
        first_zero_crossing = 376
        
        """
        fig, ax1 = plt.subplots()
        
        ax1.scatter(self.data[first_zero_crossing:, self._time_index]/self.one_year_et+2000, 2*np.arctan(1/self.data[first_zero_crossing:,self._dist_index])*180/np.pi, marker = '.')
        ax1.set_xlabel('Year')
        ax1.set_ylabel('Apparent Size of the Zodiacal Cloud [°]')
        plt.savefig('zodiacsize.pdf')

        
        # Create a twin Axes for the second x-axis at the top
        ax2 = ax1.twiny()
        
        flyby = 1992.17646473
        aphelia = [1998.28180604, 2004.52656168]
        maxima = [1995+238/365.2425, 2001+303.5/365.2425]
        minima = [1994+281/365.2425, 2000+353/365.2425, 2007+69/365.2425]
        
        
        # Add specific times to the top x-axis
        specific_times = [flyby, minima[0], maxima[0], aphelia[0], minima[1], maxima[1], aphelia[1], minima[2]]  # Add the specific times you want to annotate
        specific_phrases = ['Jupiter Fly-By', 'South Pole', 'North Pole', 'Aphelion', 'South Pole', 'North Pole', 'Aphelion', 'South Pole']  # Corresponding phrases
        
        ax2.set_xticks(specific_times)
        ax2.set_xticklabels(specific_phrases, rotation=45, ha='left')  # Adjust rotation and ha
        
        ax2.set_xlim(ax1.get_xlim())
        
        plt.show()
        
        """
        
        with open('zodiac.dat', 'w') as f:
            for i in range(first_zero_crossing,len(self.data[:, self._time_index])):
                f.write(str(self.data[i, self._time_index]/self.one_year_et+2000)+','+str(2*np.arctan(1/self.data[i,self._dist_index])*180/np.pi)+'\n')

    def _plot_zodiac_direction(self):
        
        zodiac_lon = (self._new_data[:, indices['LON']]-90)%360
        
        no_zodiac_beta = []
        
        
        lon = self._new_data[:, indices['solar_lon_index']]
        lat = self._new_data[:, indices['solar_lat_index']]
        detector_zodiac_angles = []
        et = self._new_data[:, self._time_index]
        for i in range(len(self._new_data[:,0])):
            [stateUlysses, ltime] = spice.spkezr('ULYSSES',  et[i],      'ECLIPJ2000', 'NONE', 'SUN')
            velUlysses = stateUlysses[3:]
            
            velImpact = self._new_data[i, indices['velocity_index']]
            
            pointingDetector = spice.latrec(1, lon[i]*2*np.pi/360, lat[i]*2*np.pi/360)
            
            vel = velUlysses-velImpact*pointingDetector
            
            zodiacDirection = spice.latrec(1, zodiac_lon[i], 0)
            
            angle0 = spice.vsep(vel, zodiacDirection)*360/(2*np.pi)
            detector_zodiac_angles.append(angle0)
            
            no_zodiac_beta.append(angle0 > 50)
                
                
            
        self._no_zodiac_beta = no_zodiac_beta
        
        lon_crit = [detector_zodiac_angles[i] > 50 for i in range(len(zodiac_lon))]
        color_lon =  ['red' if lon_crit[i] else 'black' for i in range(len(zodiac_lon))]
        
        
        fig, ax1 = plt.subplots()
        
        ax1.scatter(self.data[:, self._time_index]/self.one_year_et+2000, self.data[:,indices['LAT']], label = 'all', alpha = 0.5, marker = '.', s= 1)
        ax1.plot(self._new_data[:, self._time_index]/self.one_year_et+2000, np.zeros(len(self._new_data[:, self._time_index])), color = 'red')
        ax1.scatter(self._new_data[:, self._time_index]/self.one_year_et+2000,self._new_data[:, indices['LAT']], color = color_lon, marker = 'x')
        black_dot = mlines.Line2D([], [], color='black', marker='x', ls = '', label='Potential zodiac particles')
        red_dot = mlines.Line2D([], [], color='red', marker='x', ls = '', label='Other beta particles')
        ax1.legend(handles=[black_dot, red_dot], loc = 'lower left')
        ax1.set_xlabel('Year')
        ax1.set_ylabel('Ulysses Ecliptic Latitude [°]')
        
        # Create a twin Axes for the second x-axis at the top
        ax2 = ax1.twiny()
        
        flyby = 1992.17646473
        aphelia = [1998.28180604, 2004.52656168]
        maxima = [1995+238/365.2425, 2001+303.5/365.2425]
        minima = [1994+281/365.2425, 2000+353/365.2425, 2007+69/365.2425]
        
        
        # Add specific times to the top x-axis
        specific_times = [flyby, minima[0], maxima[0], aphelia[0], minima[1], maxima[1], aphelia[1], minima[2]]  # Add the specific times you want to annotate
        specific_phrases = ['Jupiter Fly-By', 'South Pole', 'North Pole', 'Aphelion', 'South Pole', 'North Pole', 'Aphelion', 'South Pole']  # Corresponding phrases
        
        ax2.set_xticks(specific_times)
        ax2.set_xticklabels(specific_phrases, rotation=45, ha='left')  # Adjust rotation and ha
        
        ax2.set_xlim(ax1.get_xlim())
        plt.tight_layout()
        plt.savefig('zodiac.pdf')
        plt.show()
        


    def _calculate_zero_crossings_times(self) -> list:
        #https://stackoverflow.com/questions/3843017/efficiently-detect-sign-changes-in-python
        zero_crossings = np.where(np.diff(np.signbit(self.raw_data[:,indices['LAT']])))[0]
        zero_crossings_times = self.raw_data[zero_crossings, self._time_index]
        self._zero_crossings_times = zero_crossings_times
        
    
    def _count_and_print_north_fraction(self):
        count0 = 0
        countoutsideecl = 0
        count999 = 0
        countbeta = 0
        countstreams = 0
        countnostreams = 0
        countnostreamstot = 0
        countnotinterstellar = 0
        countnotinterstellartot = 0
        countnotinterstellarstreams = 0
        countnotinterstellarstreamstot = 0
        countinterstellar = 0
        countinterstellar_noeclip = 0
        countinterstellarnorth = 0
        first_not_north_index = 378
        
        for i in range(len(self.data)):
            if self.data[i,indices['LAT']] > 0:
                count0 += 1
                
        for i in range(first_not_north_index,len(self.data)):
            if self.data[i,indices['LAT']] > 0:
                countoutsideecl += 1
                
        for i in range(len(self.data_without_999)):
            if self.data_without_999[i,indices['LAT']] > 0:
                count999 += 1
                
        for i in range(len(self.data_without_999)):
            condition_not_streams = True
            for interval in self._tobias_streams:
                if self.data_without_999[i,self._time_index]/self.one_year_et+2000 >= interval[0] and self.data_without_999[i,self._time_index]/self.one_year_et+2000 <= interval[1]:
                    condition_not_streams = False
                    countstreams += 1
            if condition_not_streams:
                if self.data_without_999[i,indices['LAT']] > 0:
                    countnostreams += 1
                countnostreamstot += 1
            
                if self._condition_not_interstellar[i]:
                    if self.data_without_999[i,indices['LAT']] > 0:
                        countnotinterstellarstreams += 1
                    countnotinterstellarstreamstot += 1
        
        print('No 999 streams:', countstreams)
        
        for i in range(len(self.data_without_999)):
            if self._condition_not_interstellar[i]:
                if self.data_without_999[i,indices['LAT']] > 0:
                    countnotinterstellar += 1
                countnotinterstellartot += 1
            else:
                countinterstellar += 1
                if np.abs(self.data_without_999[i,indices['LAT']]) > 5:
                    countinterstellar_noeclip += 1
                    if self.data_without_999[i,indices['LAT']] > 0:
                        countinterstellarnorth += 1
        print('No 999 Interstellar:', countinterstellar, 'No 999 Interstellar no eclip:', countinterstellar_noeclip, 'No 999 Interstellar north', countinterstellarnorth, 'No 999 Interstellar no eclip north frac', countinterstellarnorth/countinterstellar)
        
        for i in range(len(self._new_data)):
            if self._new_data[i,indices['LAT']] > 0:
                countbeta += 1
                
        print('All data north fraction =', count0/len(self.data),
              '\nAfter flyby north fraction =', countoutsideecl/len(range(first_not_north_index,len(self.data))),
              '\nNo 999 north fraction =', count999/len(self.data_without_999),
              '\nNo 999 and streams north fraction =', countnostreams/countnostreamstot,
              '\nNo 999 and interstellar north fraction =', countnotinterstellar/countnotinterstellartot,
              '\nNo 999 and interstellar and streams north fraction =', countnotinterstellarstreams/countnotinterstellarstreamstot,
              '\nBeta north fraction =', countbeta/len(self._new_data))

    def _plot_effective_area_with_streams(self):
            old_eff_area_data = np.loadtxt(self.eff_area_file, delimiter = ',')
            if self.eff_area_file == 'DefaultDataset.csv':
                old_eff_area_data[:,1] = old_eff_area_data[:,1]*10000
                
            x = np.linspace(old_eff_area_data[0,0], old_eff_area_data[-1,0], num = 100000)
            eff_area_data = np.array([x, np.interp(x, old_eff_area_data[:,0], old_eff_area_data[:,1])])
            
            streams = []
            for i in range(len(self._streams1)):
                streams.append(self._streams1[i])
            for i in range(len(self._streams2)):
                streams.append(self._streams2[i])
                
            self._peter_streams = streams
            
            #Check when Ulysses is within a Jupiter stream
            
            fig, ax1 = plt.subplots()
            
            one_time_legend_flag = True
            no_stream_index_list0 = range(len(eff_area_data[0,:]))
            for interval in streams:
                cond0 = np.where(eff_area_data[0,:] >= interval[0])
                cond1 = np.where(eff_area_data[0,:] <= interval[1])
                cond = np.intersect1d(cond0, cond1)
                if one_time_legend_flag:
                    ax1.plot(eff_area_data[0,cond], 110+0*eff_area_data[1,cond], label = 'Peter Jupiter stream', color = 'black')
                    one_time_legend_flag = False
                else:
                    ax1.plot(eff_area_data[0,cond], 110+0*eff_area_data[1,cond], color = 'black')
                no_stream_index_list0 = list(set(no_stream_index_list0) - set(cond))            
            
            one_time_legend_flag = True
            no_stream_index_list1 = range(len(eff_area_data[0,:]))
            for interval in self._tobias_streams:
                cond0 = np.where(eff_area_data[0,:] >= interval[0])
                cond1 = np.where(eff_area_data[0,:] <= interval[1])
                cond = np.intersect1d(cond0, cond1)
                if one_time_legend_flag:
                    ax1.plot(eff_area_data[0,cond], 100+0*eff_area_data[1,cond], label = 'Tobias Jupiter stream', color = 'green')
                    one_time_legend_flag = False
                else:
                    ax1.plot(eff_area_data[0,cond], 100+0*eff_area_data[1,cond], color = 'green')
                no_stream_index_list1 = list(set(no_stream_index_list1) - set(cond))
            
            ax1.set_xlabel('Year')
            ax1.set_ylabel('Rotation angle [°]')
            
            # Create an array of colors based on the third data value
            colors = ['red' if val > 7 else 'blue' for val in self.data_without_999[:,indices['IA']]]
            
            # Scatter plot with colors based on the third data value
            ax1.scatter(self.data_without_999[:,self._time_index]/self.one_year_et+2000,
                         self.data_without_999[:,indices['rotation_angle_index']],
                         c=colors)
            
            streams_data = np.loadtxt('stststreamscomp.dat', delimiter=',')
            
            streams_locations = []
            for line in streams_data:
                if line[0] < 2000:
                    streams_locations.append(np.linspace(line[-1]-2*line[2]/24/365,line[-1]+2*line[2]/24/365))
                else:
                    streams_locations.append(np.linspace(line[-1]-2*line[3]/365,line[-1]+2*line[3]/365))
            
            for i, entry in enumerate(streams_locations):
                ax1.plot(entry,105+0*entry, color = 'violet')
                ax1.scatter(streams_data[i,-1], 105, color = 'violet', marker = 'x')
            
            ax1.legend()
            
            # Create a twin Axes for the second x-axis at the top
            ax2 = ax1.twiny()
            
            flyby = 1992.17646473
            aphelia = [1998.28180604, 2004.52656168]
            maxima = [1995+238/365.2425, 2001+303.5/365.2425]
            minima = [1994+281/365.2425, 2000+353/365.2425, 2007+69/365.2425]
            
            
            # Add specific times to the top x-axis
            specific_times = [flyby, minima[0], maxima[0], aphelia[0], minima[1], maxima[1], aphelia[1], minima[2]]  # Add the specific times you want to annotate
            specific_phrases = ['Jupiter Fly-By', 'South Pole', 'North Pole', 'Aphelion', 'South Pole', 'North Pole', 'Aphelion', 'South Pole']  # Corresponding phrases
            
            ax2.set_xticks(specific_times)
            ax2.set_xticklabels(specific_phrases, rotation=45, ha='left')  # Adjust rotation and ha
            
            ax2.set_xlim(ax1.get_xlim())
            plt.show()
            
            fig, ax1 = plt.subplots()
            
            one_time_legend_flag = True
            ax1.plot(eff_area_data[0,:], eff_area_data[1,:], color = 'black', alpha = 0.5)
            for interval in self._tobias_streams:
                cond0 = np.where(eff_area_data[0,:] >= interval[0])
                cond1 = np.where(eff_area_data[0,:] <= interval[1])
                cond = np.intersect1d(cond0, cond1)
                if one_time_legend_flag:
                    ax1.plot(eff_area_data[0,cond], eff_area_data[1,cond], label = 'Jupiter stream', color = 'green', lw = 2)
                    one_time_legend_flag = False
                else:
                    ax1.plot(eff_area_data[0,cond], eff_area_data[1,cond], color = 'green', lw = 2)
            
            ax1.set_xlabel('Year')
            ax1.set_ylabel('Effective Area [cm$^2$]')
            
            
            ax1.legend()
            
            # Create a twin Axes for the second x-axis at the top
            ax2 = ax1.twiny()
            
            flyby = 1992.17646473
            aphelia = [1998.28180604, 2004.52656168]
            maxima = [1995+238/365.2425, 2001+303.5/365.2425]
            minima = [1994+281/365.2425, 2000+353/365.2425, 2007+69/365.2425]
            
            
            # Add specific times to the top x-axis
            specific_times = [flyby, minima[0], maxima[0], aphelia[0], minima[1], maxima[1], aphelia[1], minima[2]]  # Add the specific times you want to annotate
            specific_phrases = ['Jupiter Fly-By', 'South Pole', 'North Pole', 'Aphelion', 'South Pole', 'North Pole', 'Aphelion', 'South Pole']  # Corresponding phrases
            
            ax2.set_xticks(specific_times)
            ax2.set_xticklabels(specific_phrases, rotation=45, ha='left')  # Adjust rotation and ha
            
            ax2.set_xlim(ax1.get_xlim())
            plt.tight_layout()
            plt.savefig('streamsFinal.pdf')
            plt.show()

    
    def _plot_flux(self):
        mean_dist_array, flux_bins, times_array = self._calculate_mean_eff_area_and_flux()
        print(flux_bins*10000)

        label_array = ['Pre-Jupiter fly-by', 'South', 'North', None, None, None, None]
        color_array = ['black', 'blue', 'red', 'blue', 'red', 'blue', 'red']
        
        fig, ax1 = plt.subplots()
        for i in range(len(color_array)):
            #plt.scatter(mean_dist_array[i], flux_bins[i]*10000, label = label_array[i], color = color_array[i], s=100)
            if color_array[i] != 'blue':
                plt.scatter(times_array[i], flux_bins[i]*10000, label = label_array[i], color = color_array[i])
        ax1.set_xlabel('Year')
        ax1.set_ylabel(r'Flux [1/(m$^2\cdot$s)]')
        ax1.set_ylim(0,0.00012)
        ax1.legend()
        
        ax2 = ax1.twiny()
        
        flyby = 1992.17646473
        aphelia = [1998.28180604, 2004.52656168]
        maxima = [1995+238/365.2425, 2001+303.5/365.2425]
        minima = [1994+281/365.2425, 2000+353/365.2425, 2007+69/365.2425]
        
        
        # Add specific times to the top x-axis
        specific_times = [flyby, minima[0], maxima[0], aphelia[0], minima[1], maxima[1], aphelia[1], minima[2]]  # Add the specific times you want to annotate
        specific_phrases = ['Jupiter Fly-By', 'South Pole', 'North Pole', 'Aphelion', 'South Pole', 'North Pole', 'Aphelion', 'South Pole']  # Corresponding phrases
        
        
        ax2.set_xticks(specific_times)
        ax2.set_xticklabels(specific_phrases, rotation=45, ha='left')  # Adjust rotation and ha
        
        ax2.set_xlim(ax1.get_xlim())
        
        plt.tight_layout()
        plt.savefig('flux.pdf')
        plt.show()
        
        plt.scatter((1968+1972)/2, 0.0006, label = 'Pioneer 8&9', color = 'green', marker = 'x')
        plt.scatter((1991+1992)/2, 0.00049, label = 'Ulysses Pre-Jupiter fly-by', color = 'black', marker = '+')
        plt.errorbar((2019+2019.5)/2, (0.00003+0.00007)/2, yerr = (0.00003+0.00007)/2-0.00003, fmt = 'none', label = 'Parker Solar Probe')
        plt.scatter((2020.5+2022)/2, 0.00011, label = 'Solar Orbiter', color = 'red')
        plt.xlabel('Year')
        plt.ylabel(r'Normalized Flux at $1\,$au [1/(m$^2\cdot$s)]')
        plt.legend()
        plt.ylim(0,0.0007)
        plt.tight_layout()
        plt.savefig('FluxComp1au.pdf')
        plt.show()
        
        
    #Bins over pre-fly-by, north sections an south sections and calculates fluxes and mean effective area of bin
    def _calculate_mean_eff_area_and_flux(self) -> (list, list):        
        flux_bins = []
        mean_dist_array = []
        inv_effArea_time = []
        eff_area = []
        times = []
        times_array = []
        north_polar_traverse_times = []
        
        #Does the same thing three times, for pre-Jupiter, for everything but the last semi-orbit, and for the last semi-orbit
        
        #Number 1
        dist_array = []
        
        for i in range(len(self._new_data)):
            if self._new_data[i, self._time_index]<=self._zero_crossings_times[0]:
                if self._no_zodiac_beta[i] == True:
                    dist_array.append(self._beta_dist[i])
                    eff_area.append(self._eff_area_res[i])
                    times.append(self._new_data[i, self._time_index])
        dist_array = np.array(dist_array)
        mean_dist_array.append(np.mean(dist_array))
        print(len(dist_array))
        mean_eff_area = np.mean(np.array(eff_area))
        flux_bins.append(len(dist_array)/(mean_eff_area*(times[-1]-times[0])))
        inv_effArea_time.append(1/(mean_eff_area*(times[-1]-times[0])))
        times_array.append(np.mean(times)/self.one_year_et+2000)
        
        #print('0.972199293*dist^1.6',0.972199293*np.mean(dist_array)**(2-0.44862417),0.972199293*np.mean(dist_array**(2-0.44862417)))
        print('0.97*dist^1.6',0.97*np.mean(dist_array)**(2-0.4),0.97*np.mean(dist_array**(2-0.4)))
        print('0.81*dist^1.6',1.05*2.2**(2-0.4), 0.61*2.2**(2-0.4), 0.76*2.2**(2-0.4))
        
        #Number 2 to n-1
        for k in range(len(self._zero_crossings_times)-1):
            dist_array = []
            eff_area = []
            times = []
            
            idx = find_nearest_idx(self._new_data[:, self._time_index], self._zero_crossings_times[k])+1
            
            for i in range(idx, len(self._new_data)):
                if self._new_data[i, self._time_index]<=self._zero_crossings_times[k+1]:
                    if self._no_zodiac_beta[i] == True:
                        dist_array.append(self._beta_dist[i])
                        eff_area.append(self._eff_area_res[i])
                        times.append(self._new_data[i, self._time_index])
            if k == 3:
                dist_array.pop(-1)
                dist_array.pop(-1)
                eff_area.pop(-1)
                eff_area.pop(-1)
                times.pop(-1)
                times.pop(-1)
            dist_array = np.array(dist_array)
            mean_dist_array.append(np.mean(dist_array))
            print(len(dist_array))
            mean_eff_area = np.mean(np.array(eff_area))
            if k == 2:
                flux_bins.append(0)
                inv_effArea_time.append(np.nan)
                times_array.append(np.mean(times)/self.one_year_et+2000)
            else:
                try:
                    flux_bins.append(len(dist_array)/(mean_eff_area*(times[-1]-times[0])))
                    inv_effArea_time.append(1/(mean_eff_area*(times[-1]-times[0])))
                    times_array.append(np.mean(times)/self.one_year_et+2000)
                    north_polar_traverse_times.append(times[-1]-times[0])
                except:
                    flux_bins.append(0)
                    inv_effArea_time.append(np.nan)
                    times_array.append(np.mean(times)/self.one_year_et+2000)
            
        #Number n 
        dist_array = []
        eff_area = []
        times = []
        
        idx = find_nearest_idx(self._new_data[:, self._time_index], self._zero_crossings_times[-1])
        
        for i in range(idx, len(self._new_data)):
            if self._no_zodiac_beta[i] == True:
                    dist_array.append(self._beta_dist[i])
                    eff_area.append(self._eff_area_res[i])
                    times.append(self._new_data[i, self._time_index])
        dist_array = np.array(dist_array)
        mean_dist_array.append(np.mean(dist_array))
        print(len(dist_array))
        mean_eff_area = np.mean(np.array(eff_area))
        flux_bins.append(len(dist_array)/(mean_eff_area*(times[-1]-times[0])))
        inv_effArea_time.append(1/(mean_eff_area*(times[-1]-times[0])))
        times_array.append(np.mean(times)/self.one_year_et+2000)
        north_polar_traverse_times.append(times[-1]-times[0])
        
        mean_north_polar_traverse_time = np.mean(np.array(north_polar_traverse_times))
        
        self._mean_north_polar_traverse_time = mean_north_polar_traverse_time
        
        self._effective_area_with_lat()
        
        count = 0
        for i in range(len(inv_effArea_time)):
            if np.isnan(inv_effArea_time[i]):
                inv_effArea_time[i] = 1/(self._mean_north_polar_traverse_time*self._south_polar_traverse_eff_area[count])
                count += 1
                
        
        mean_dist_array = np.array(mean_dist_array)
        flux_bins = np.array(flux_bins)
        inv_effArea_time = np.array(inv_effArea_time)
        avg_flux = (flux_bins[2]+flux_bins[4]+flux_bins[6])*10000/3
        expected_number = avg_flux/inv_effArea_time
        print(avg_flux)
        print(expected_number/10000)
        
        
        return mean_dist_array, flux_bins, times_array
    
    def _effective_area_with_lat(self):
        zero_crossings_times_in_epoch = self._zero_crossings_times/self.one_year_et+2000
        eff_area_data = np.loadtxt(self.eff_area_file, delimiter = ',')
        if self.eff_area_file == 'DefaultDataset.csv':
            eff_area_data[:,1] = eff_area_data[:,1]*10000
            
        #Check in which hemisphere Ulysses currently is, plot that, and calculate the average effective area for that bin
        #Does the same thing three times, for pre-Jupiter, for everything but the last semi-orbit, and for the last semi-orbit
        
        fig, ax1 = plt.subplots()
        
        #Number 1
        cond = np.where(eff_area_data[:,0] < zero_crossings_times_in_epoch[0])
        cond = np.intersect1d(cond, cond)
        ax1.plot(eff_area_data[cond,0], eff_area_data[cond,1], label = 'Pre-Jupiter fly-by', color = 'black')
        
        south_polar_traverse_eff_area = []
        #Number 2 to n-1
        for i in range(len(zero_crossings_times_in_epoch)-1):
            if i == 0:
                label = 'South'
            elif i == 1:
                label = 'North'
            else:
                label = None
            if i % 2 == 0:
                color = 'blue'
            else:
                color = 'red'
            cond0 = np.where(eff_area_data[:,0] > zero_crossings_times_in_epoch[i])
            cond1 = np.where(eff_area_data[:,0] < zero_crossings_times_in_epoch[i+1])
            cond = np.intersect1d(cond0, cond1)
            if color == 'blue':
                cond2 = np.where(eff_area_data[:,0] > zero_crossings_times_in_epoch[i+1]-self._mean_north_polar_traverse_time/self.one_year_et)
                cond3 = np.intersect1d(cond, cond2)
                south_polar_traverse_eff_area.append(np.mean(eff_area_data[cond3,1]))
            ax1.plot(eff_area_data[cond,0], eff_area_data[cond,1], label = label, color = color)
            
        self._south_polar_traverse_eff_area = np.array(south_polar_traverse_eff_area)
        
        #Number n
        cond = np.where(eff_area_data[:,0] > zero_crossings_times_in_epoch[-1])
        cond = np.intersect1d(cond, cond)
        ax1.plot(eff_area_data[cond,0], eff_area_data[cond,1], color = 'red')
        ax1.scatter(self._new_data[:, self._time_index]/self.one_year_et+2000, self._eff_area_res, label = 'beta', marker = 'x', color = 'green')
        ax1.set_xlabel('Year')
        ax1.set_ylabel('Effective Area [cm$^2$]')
        
        southern = np.where(self._new_data[:,indices['LAT']] < 0)
        late = np.where(self._new_data[:, self._time_index]/self.one_year_et+2000 > 1993)
        far_away = np.where(self._new_data[:, self._dist_index] > 3)
        southern_late = np.intersect1d(southern,late)
        far_late = np.intersect1d(far_away,late)
        ax1.scatter(self._new_data[far_late, self._time_index]/self.one_year_et+2000, self._eff_area_res[far_late], label = 'outer Solar System beta', marker = 's')
        ax1.scatter(self._new_data[southern_late, self._time_index]/self.one_year_et+2000, self._eff_area_res[southern_late], label = 'southern beta', marker = 's')
        
        ax1.legend()
        
        # Create a twin Axes for the second x-axis at the top
        ax2 = ax1.twiny()
        
        flyby = 1992.17646473
        aphelia = [1998.28180604, 2004.52656168]
        maxima = [1995+238/365.2425, 2001+303.5/365.2425]
        minima = [1994+281/365.2425, 2000+353/365.2425, 2007+69/365.2425]
        
        
        # Add specific times to the top x-axis
        specific_times = [flyby, minima[0], maxima[0], aphelia[0], minima[1], maxima[1], aphelia[1], minima[2]]  # Add the specific times you want to annotate
        specific_phrases = ['Jupiter Fly-By', 'South Pole', 'North Pole', 'Aphelion', 'South Pole', 'North Pole', 'Aphelion', 'South Pole']  # Corresponding phrases
        
        ax2.set_xticks(specific_times)
        ax2.set_xticklabels(specific_phrases, rotation=45, ha='left')  # Adjust rotation and ha
        
        ax2.set_xlim(ax1.get_xlim())
        plt.tight_layout()
        plt.savefig('effArea.pdf')
        plt.show()
        
        fig, ax1 = plt.subplots()
        
        #Number 1
        cond = np.where(eff_area_data[:,0] < zero_crossings_times_in_epoch[0])
        cond = np.intersect1d(cond, cond)
        ax1.plot(eff_area_data[cond,0], eff_area_data[cond,1], label = 'Pre-Jupiter fly-by', color = 'black')
        
        south_polar_traverse_eff_area = []
        #Number 2 to n-1
        for i in range(len(zero_crossings_times_in_epoch)-1):
            if i == 0:
                label = 'South'
            elif i == 1:
                label = 'North'
            else:
                label = None
            if i % 2 == 0:
                color = 'blue'
            else:
                color = 'red'
            cond0 = np.where(eff_area_data[:,0] > zero_crossings_times_in_epoch[i])
            cond1 = np.where(eff_area_data[:,0] < zero_crossings_times_in_epoch[i+1])
            cond = np.intersect1d(cond0, cond1)
            if color == 'blue':
                cond2 = np.where(eff_area_data[:,0] > zero_crossings_times_in_epoch[i+1]-self._mean_north_polar_traverse_time/self.one_year_et)
                cond3 = np.intersect1d(cond, cond2)
                south_polar_traverse_eff_area.append(np.mean(eff_area_data[cond3,1]))
            ax1.plot(eff_area_data[cond,0], eff_area_data[cond,1], label = label, color = color)
            
        self._south_polar_traverse_eff_area = np.array(south_polar_traverse_eff_area)
        
        #Number n
        cond = np.where(eff_area_data[:,0] > zero_crossings_times_in_epoch[-1])
        cond = np.intersect1d(cond, cond)
        ax1.plot(eff_area_data[cond,0], eff_area_data[cond,1], color = 'red')
        ax1.set_xlabel('Year')
        ax1.set_ylabel('Effective Area [cm$^2$]')
        ax1.legend()
        
        # Create a twin Axes for the second x-axis at the top
        ax2 = ax1.twiny()
        
        flyby = 1992.17646473
        aphelia = [1998.28180604, 2004.52656168]
        maxima = [1995+238/365.2425, 2001+303.5/365.2425]
        minima = [1994+281/365.2425, 2000+353/365.2425, 2007+69/365.2425]
        
        
        # Add specific times to the top x-axis
        specific_times = [flyby, minima[0], maxima[0], aphelia[0], minima[1], maxima[1], aphelia[1], minima[2]]  # Add the specific times you want to annotate
        specific_phrases = ['Jupiter Fly-By', 'South Pole', 'North Pole', 'Aphelion', 'South Pole', 'North Pole', 'Aphelion', 'South Pole']  # Corresponding phrases
        
        ax2.set_xticks(specific_times)
        ax2.set_xticklabels(specific_phrases, rotation=45, ha='left')  # Adjust rotation and ha
        
        ax2.set_xlim(ax1.get_xlim())
        
        plt.tight_layout()
        plt.savefig('effAreaPure.pdf')
        plt.show()
    

    def __init__(self):     
        self._one_time_wehry_flag = False
        self._time_index = 0
        self._dist_index = 1
        self._index_index = 2
        
        self._wehry_velocity = 20
        self._wehry_angle = 50
        #self._wehry_angle = 10
        self._streams1 = [[1991.727,1991.740],[1991.948,1991.953],[1991.978,1991.983],[1992.017,1992.021],[1992.048,1992.056],[1992.192,1992.196],[1992.268,1992.275],[1992.342,1992.349],[1992.417,1992.435],[1992.670,1992.685],[1992.796,1992.811]]
        self._streams2 = [[2002.905,2002.917],[2003.515,2003.535],[2003.643,2003.663],[2003.717,2003.742],[2003.778,2003.802],[2003.862,2003.867],[2003.919,2003.928],[2003.993,2004.007],[2004.063,2004.078],[2004.138,2004.142],[2004.207,2004.231],[2004.410,2004.442],[2004.450,2004.510],[2004.544,2004.574],[2004.582,2004.603],[2004.625,2004.654],[2004.825,2004.833],[2004.907,2004.914],[2004.989,2005.001],[2005.113,2005.135],[2005.210,2005.236],[2005.474,2005.487],[2005.565,2005.583],[2005.615,2005.642]]
        self._instrument = [ [2000.4905, 2000.49625], [2002.23297, 2002.2700], [2002.917692,2003.422021], [2003.5733, 2003.642642], [2004.918349, 2004.923907]]
        
        #self._tobias_streams = [[ 1991.742 , 1991.7515 ],[ 1991.961 , 1991.96273 ],[ 1991.991 , 1991.996 ],[ 1992.031 , 1992.0332 ],[ 1992.052 , 1992.075 ],[ 1992.205 , 1992.2073 ],[ 1992.278 , 1992.29 ],[ 1992.35 , 1992.363 ],[ 1992.428 , 1992.447 ],[ 1992.66 , 1992.71 ],[ 1992.806 , 1992.824 ],[ 2002.897 , 2002.908 ],[ 2003.505 , 2003.525 ],[ 2003.633 , 2003.65 ],[ 2003.709 , 2003.715 ],[ 2003.768 , 2003.791 ],[ 2003.851 , 2003.855 ],[ 2003.91 , 2003.92 ],[ 2003.98 , 2003.999 ],[ 2004.056 , 2004.066 ],[ 2004.118, 2004.13 ],[ 2004.195 , 2004.219 ],[ 2004.394 , 2004.512 ],[ 2004.565 , 2004.595 ],[ 2004.61 , 2004.66 ],[ 2004.675 , 2004.695 ],[ 2004.804 , 2004.824 ],[ 2004.888 , 2004.898 ],[ 2004.972 , 2004.99 ],[ 2005.1 , 2005.113 ],[ 2005.197 , 2005.223 ],[ 2005.319 , 2005.349 ],[ 2005.457 , 2005.481 ],[ 2005.553 , 2005.574 ],[ 2005.609 , 2005.628 ]]
        #self._tobias_streams = [[ 1991.727 , 1992.824 ],[ 2002.897 , 2002.908 ],[ 2003.505 , 2003.525 ],[ 2003.633 , 2003.65 ],[ 2003.709 , 2003.715 ],[ 2003.768 , 2003.791 ],[ 2003.851 , 2003.855 ],[ 2003.91 , 2003.92 ],[ 2003.98 , 2003.999 ],[ 2004.056 , 2004.066 ],[ 2004.118, 2004.13 ],[ 2004.195 , 2004.219 ],[ 2004.394 , 2004.512 ],[ 2004.565 , 2004.595 ],[ 2004.61 , 2004.66 ],[ 2004.675 , 2004.695 ],[ 2004.804 , 2004.824 ],[ 2004.888 , 2004.898 ],[ 2004.972 , 2004.99 ],[ 2005.1 , 2005.113 ],[ 2005.197 , 2005.223 ],[ 2005.319 , 2005.349 ],[ 2005.457 , 2005.481 ],[ 2005.553 , 2005.574 ],[ 2005.609 , 2005.628 ]]
        self._tobias_streams = [[ 1991.852 , 1992.391 ],[ 2002.897 , 2002.908 ],[ 2003.505 , 2003.525 ],[ 2003.633 , 2003.65 ],[ 2003.709 , 2003.715 ],[ 2003.768 , 2003.791 ],[ 2003.851 , 2003.855 ],[ 2003.91 , 2003.92 ],[ 2003.98 , 2003.999 ],[ 2004.056 , 2004.066 ],[ 2004.118, 2004.13 ],[ 2004.195 , 2004.219 ],[ 2004.394 , 2004.512 ],[ 2004.565 , 2004.595 ],[ 2004.61 , 2004.66 ],[ 2004.675 , 2004.695 ],[ 2004.804 , 2004.824 ],[ 2004.888 , 2004.898 ],[ 2004.972 , 2004.99 ],[ 2005.1 , 2005.113 ],[ 2005.197 , 2005.223 ],[ 2005.319 , 2005.349 ],[ 2005.457 , 2005.481 ],[ 2005.553 , 2005.574 ],[ 2005.609 , 2005.628 ]]

        self._interstellar_ecliptic_lon = 252
        self._interstellar_ecliptic_lat = 2.5
        self._tolerance = 70
        self._interstellar_min_vel = 13
        self._interstellar_max_vel = 39
        self._interstellar_min_mass = 2.5e-14
        self._interstellar_max_mass = 1e-12
        self._wehry_beta_particle_indices = [4,6,8,18,19,29,31,34,35,36,38,43,48,49,59,61,66,72,74,76,80,83,86,94,1032,1080,1145,1165,1410,1412,1421,1422,1427,1428,1429,1431,1433,1436,1438,1440,1442,1449,1450,1452,1455,1465,1984,2001,2003,2010,2012,2024,2034,2035,2048,2049,2051,2052,2053,2054,2055,2060]

        