import csv
import re

# Input CSV file name
input_file = '/home/steint/Documents/sens_isd26/sens_isd_26km_apex_pm0.txt'

# Output CSV file name
output_file = '/home/steint/Documents/sens_isd26/sens_isd_26km_apex_pm0_fixed.txt'

# Open the input CSV file for reading and the output CSV file for writing
with open(input_file, 'r') as input_csv, open(output_file, 'w', newline='') as output_csv:
    # Create CSV reader and writer objects
    csv_reader = csv.reader(input_csv)
    csv_writer = csv.writer(output_csv)

    # Process and write the data
    for row in csv_reader:
        # Split the first column value by a space and create a new row
        second_column_value = row[1]
        second_column_values = re.findall(r'\S+', second_column_value)
        new_row = [row[0], second_column_values[0], second_column_values[1]] + row[2:]  # Concatenate the split values with the rest of the row
        csv_writer.writerow(new_row)

print(f"CSV file '{input_file}' has been modified and saved as '{output_file}'.")


# Input CSV file name
input_file = '/home/steint/Documents/sens_isd26/sens_isd_26km_apex_p20.txt'

# Output CSV file name
output_file = '/home/steint/Documents/sens_isd26/sens_isd_26km_apex_p20_fixed.txt'

# Open the input CSV file for reading and the output CSV file for writing
with open(input_file, 'r') as input_csv, open(output_file, 'w', newline='') as output_csv:
    # Create CSV reader and writer objects
    csv_reader = csv.reader(input_csv)
    csv_writer = csv.writer(output_csv)

    # Process and write the data
    for row in csv_reader:
        # Split the first column value by a space and create a new row
        second_column_value = row[1]
        second_column_values = re.findall(r'\S+', second_column_value)
        new_row = [row[0], second_column_values[0], second_column_values[1]] + row[2:]  # Concatenate the split values with the rest of the row
        csv_writer.writerow(new_row)

print(f"CSV file '{input_file}' has been modified and saved as '{output_file}'.")

# Input CSV file name
input_file = '/home/steint/Documents/sens_isd26/sens_isd_26km_apex_m20.txt'

# Output CSV file name
output_file = '/home/steint/Documents/sens_isd26/sens_isd_26km_apex_m20_fixed.txt'

# Open the input CSV file for reading and the output CSV file for writing
with open(input_file, 'r') as input_csv, open(output_file, 'w', newline='') as output_csv:
    # Create CSV reader and writer objects
    csv_reader = csv.reader(input_csv)
    csv_writer = csv.writer(output_csv)

    # Process and write the data
    for row in csv_reader:
        # Split the first column value by a space and create a new row
        second_column_value = row[1]
        second_column_values = re.findall(r'\S+', second_column_value)
        new_row = [row[0], second_column_values[0], second_column_values[1]] + row[2:]  # Concatenate the split values with the rest of the row
        csv_writer.writerow(new_row)

print(f"CSV file '{input_file}' has been modified and saved as '{output_file}'.")

# Input CSV file name
input_file = '/home/steint/Documents/sens_wehry_2d.txt'

# Output CSV file name
output_file = '/home/steint/Documents/sens_wehry_2d_fixed.txt'

# Open the input CSV file for reading and the output CSV file for writing
with open(input_file, 'r') as input_csv, open(output_file, 'w', newline='') as output_csv:
    # Create CSV reader and writer objects
    csv_reader = csv.reader(input_csv)
    csv_writer = csv.writer(output_csv)

    # Process and write the data
    for row in csv_reader:
        # Split the first column value by a space and create a new row
        second_column_value = row[1]
        second_column_values = re.findall(r'\S+', second_column_value)
        new_row = [row[0], second_column_values[0], second_column_values[1]] + row[2:]  # Concatenate the split values with the rest of the row
        csv_writer.writerow(new_row)

print(f"CSV file '{input_file}' has been modified and saved as '{output_file}'.")
